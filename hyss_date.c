/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyss.h"
#include "hyss_streams.h"
#include "hyss_main.h"
#include "hyss_globals.h"
#include "hyss_ics.h"
#include "extslib/standard/info.h"
#include "extslib/standard/hyss_versioning.h"
#include "extslib/standard/hyss_math.h"
#include "hyss_date.h"
#include "gear_interfaces.h"
#include "lib/timelib.h"
#include "lib/timelib_private.h"
#ifndef HYSS_WIN32
#include <time.h>
#else
#include "win32/time.h"
#endif

#ifdef HYSS_WIN32
static __inline __int64 hyss_date_llabs( __int64 i ) { return i >= 0? i: -i; }
#elif defined(__GNUC__) && __GNUC__ < 3
static __inline __int64_t hyss_date_llabs( __int64_t i ) { return i >= 0 ? i : -i; }
#else
static inline long long hyss_date_llabs( long long i ) { return i >= 0 ? i : -i; }
#endif

#ifdef HYSS_WIN32
#define DATE_I64_BUF_LEN 65
# define DATE_I64A(i, s, len) _i64toa_s(i, s, len, 10)
# define DATE_A64I(i, s) i = _atoi64(s)
#else
#define DATE_I64_BUF_LEN 65
# define DATE_I64A(i, s, len) \
	do { \
		int st = snprintf(s, len, "%lld", i); \
		s[st] = '\0'; \
	} while (0);
#ifdef HAVE_ATOLL
# define DATE_A64I(i, s) i = atoll(s)
#else
# define DATE_A64I(i, s) i = strtoll(s, NULL, 10)
#endif
#endif

/* {{{ arginfo */
GEAR_BEGIN_ARG_INFO_EX(arginfo_date, 0, 0, 1)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, timestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_gmdate, 0, 0, 1)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, timestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_idate, 0, 0, 1)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, timestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strtotime, 0, 0, 1)
	GEAR_ARG_INFO(0, time)
	GEAR_ARG_INFO(0, now)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_mktime, 0, 0, 0)
	GEAR_ARG_INFO(0, hour)
	GEAR_ARG_INFO(0, min)
	GEAR_ARG_INFO(0, sec)
	GEAR_ARG_INFO(0, mon)
	GEAR_ARG_INFO(0, day)
	GEAR_ARG_INFO(0, year)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_gmmktime, 0, 0, 0)
	GEAR_ARG_INFO(0, hour)
	GEAR_ARG_INFO(0, min)
	GEAR_ARG_INFO(0, sec)
	GEAR_ARG_INFO(0, mon)
	GEAR_ARG_INFO(0, day)
	GEAR_ARG_INFO(0, year)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_checkdate, 0)
	GEAR_ARG_INFO(0, month)
	GEAR_ARG_INFO(0, day)
	GEAR_ARG_INFO(0, year)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_strftime, 0, 0, 1)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, timestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_gmstrftime, 0, 0, 1)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, timestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_time, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_localtime, 0, 0, 0)
	GEAR_ARG_INFO(0, timestamp)
	GEAR_ARG_INFO(0, associative_array)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_getdate, 0, 0, 0)
	GEAR_ARG_INFO(0, timestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_date_default_timezone_set, 0)
	GEAR_ARG_INFO(0, timezone_identifier)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_date_default_timezone_get, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_sunrise, 0, 0, 1)
	GEAR_ARG_INFO(0, time)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, latitude)
	GEAR_ARG_INFO(0, longitude)
	GEAR_ARG_INFO(0, zenith)
	GEAR_ARG_INFO(0, gmt_offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_sunset, 0, 0, 1)
	GEAR_ARG_INFO(0, time)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, latitude)
	GEAR_ARG_INFO(0, longitude)
	GEAR_ARG_INFO(0, zenith)
	GEAR_ARG_INFO(0, gmt_offset)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_date_sun_info, 0)
	GEAR_ARG_INFO(0, time)
	GEAR_ARG_INFO(0, latitude)
	GEAR_ARG_INFO(0, longitude)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_create, 0, 0, 0)
	GEAR_ARG_INFO(0, time)
	GEAR_ARG_INFO(0, timezone)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_create_from_format, 0, 0, 2)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, time)
	GEAR_ARG_OBJ_INFO(0, object, DateTimeZone, 1)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_parse, 0, 0, 1)
	GEAR_ARG_INFO(0, date)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_parse_from_format, 0, 0, 2)
	GEAR_ARG_INFO(0, format)
	GEAR_ARG_INFO(0, date)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_date_get_last_errors, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_format, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, format)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_format, 0, 0, 1)
	GEAR_ARG_INFO(0, format)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_modify, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, modify)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_modify, 0, 0, 1)
	GEAR_ARG_INFO(0, modify)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_add, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, interval)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_add, 0, 0, 1)
	GEAR_ARG_INFO(0, interval)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_sub, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, interval)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_sub, 0, 0, 1)
	GEAR_ARG_INFO(0, interval)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_timezone_get, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_date_method_timezone_get, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_timezone_set, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, timezone)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_timezone_set, 0, 0, 1)
	GEAR_ARG_INFO(0, timezone)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_offset_get, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_date_method_offset_get, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_diff, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, object2)
	GEAR_ARG_INFO(0, absolute)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_diff, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, absolute)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_time_set, 0, 0, 3)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, hour)
	GEAR_ARG_INFO(0, minute)
	GEAR_ARG_INFO(0, second)
	GEAR_ARG_INFO(0, microseconds)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_time_set, 0, 0, 2)
	GEAR_ARG_INFO(0, hour)
	GEAR_ARG_INFO(0, minute)
	GEAR_ARG_INFO(0, second)
	GEAR_ARG_INFO(0, microseconds)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_date_set, 0, 0, 4)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, year)
	GEAR_ARG_INFO(0, month)
	GEAR_ARG_INFO(0, day)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_date_set, 0, 0, 3)
	GEAR_ARG_INFO(0, year)
	GEAR_ARG_INFO(0, month)
	GEAR_ARG_INFO(0, day)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_isodate_set, 0, 0, 3)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, year)
	GEAR_ARG_INFO(0, week)
	GEAR_ARG_INFO(0, day)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_isodate_set, 0, 0, 2)
	GEAR_ARG_INFO(0, year)
	GEAR_ARG_INFO(0, week)
	GEAR_ARG_INFO(0, day)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_timestamp_set, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, unixtimestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_timestamp_set, 0, 0, 1)
	GEAR_ARG_INFO(0, unixtimestamp)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_timestamp_get, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_date_method_timestamp_get, 0)
GEAR_END_ARG_INFO()


GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_create_from_immutable, 0, 0, 1)
	GEAR_ARG_INFO(0, DateTimeImmutable)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_method_create_from_mutable, 0, 0, 1)
	GEAR_ARG_INFO(0, DateTime)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_open, 0, 0, 1)
	GEAR_ARG_INFO(0, timezone)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_name_get, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_timezone_method_name_get, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_name_from_abbr, 0, 0, 1)
	GEAR_ARG_INFO(0, abbr)
	GEAR_ARG_INFO(0, gmtoffset)
	GEAR_ARG_INFO(0, isdst)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_offset_get, 0, 0, 2)
	GEAR_ARG_OBJ_INFO(0, object, DateTimeZone, 0)
	GEAR_ARG_OBJ_INFO(0, datetime, DateTimeInterface, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_method_offset_get, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_transitions_get, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, timestamp_begin)
	GEAR_ARG_INFO(0, timestamp_end)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_method_transitions_get, 0, 0, 0)
	GEAR_ARG_INFO(0, timestamp_begin)
	GEAR_ARG_INFO(0, timestamp_end)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_location_get, 0, 0, 1)
	GEAR_ARG_INFO(0, object)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_timezone_method_location_get, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_timezone_identifiers_list, 0, 0, 0)
	GEAR_ARG_INFO(0, what)
	GEAR_ARG_INFO(0, country)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_timezone_abbreviations_list, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_timezone_version_get, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_interval_create_from_date_string, 0, 0, 1)
	GEAR_ARG_INFO(0, time)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_interval_format, 0, 0, 2)
	GEAR_ARG_INFO(0, object)
	GEAR_ARG_INFO(0, format)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO(arginfo_date_method_interval_format, 0)
	GEAR_ARG_INFO(0, format)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_period_construct, 0, 0, 3)
	GEAR_ARG_INFO(0, start)
	GEAR_ARG_INFO(0, interval)
	GEAR_ARG_INFO(0, end)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_set_state, 0, 0, 1)
	GEAR_ARG_ARRAY_INFO(0, array, 0)
GEAR_END_ARG_INFO()

GEAR_BEGIN_ARG_INFO_EX(arginfo_date_interval_construct, 0, 0, 1)
	GEAR_ARG_INFO(0, interval_spec)
GEAR_END_ARG_INFO()
/* }}} */

/* {{{ Function table */
static const gear_function_entry date_functions[] = {
	HYSS_FE(strtotime, arginfo_strtotime)
	HYSS_FE(date, arginfo_date)
	HYSS_FE(idate, arginfo_idate)
	HYSS_FE(gmdate, arginfo_gmdate)
	HYSS_FE(mktime, arginfo_mktime)
	HYSS_FE(gmmktime, arginfo_gmmktime)
	HYSS_FE(checkdate, arginfo_checkdate)

#ifdef HAVE_STRFTIME
	HYSS_FE(strftime, arginfo_strftime)
	HYSS_FE(gmstrftime, arginfo_gmstrftime)
#endif

	HYSS_FE(time, arginfo_time)
	HYSS_FE(localtime, arginfo_localtime)
	HYSS_FE(getdate, arginfo_getdate)

	/* Advanced Interface */
	HYSS_FE(date_create, arginfo_date_create)
	HYSS_FE(date_create_immutable, arginfo_date_create)
	HYSS_FE(date_create_from_format, arginfo_date_create_from_format)
	HYSS_FE(date_create_immutable_from_format, arginfo_date_create_from_format)
	HYSS_FE(date_parse, arginfo_date_parse)
	HYSS_FE(date_parse_from_format, arginfo_date_parse_from_format)
	HYSS_FE(date_get_last_errors, arginfo_date_get_last_errors)
	HYSS_FE(date_format, arginfo_date_format)
	HYSS_FE(date_modify, arginfo_date_modify)
	HYSS_FE(date_add, arginfo_date_add)
	HYSS_FE(date_sub, arginfo_date_sub)
	HYSS_FE(date_timezone_get, arginfo_date_timezone_get)
	HYSS_FE(date_timezone_set, arginfo_date_timezone_set)
	HYSS_FE(date_offset_get, arginfo_date_offset_get)
	HYSS_FE(date_diff, arginfo_date_diff)

	HYSS_FE(date_time_set, arginfo_date_time_set)
	HYSS_FE(date_date_set, arginfo_date_date_set)
	HYSS_FE(date_isodate_set, arginfo_date_isodate_set)
	HYSS_FE(date_timestamp_set, arginfo_date_timestamp_set)
	HYSS_FE(date_timestamp_get, arginfo_date_timestamp_get)

	HYSS_FE(timezone_open, arginfo_timezone_open)
	HYSS_FE(timezone_name_get, arginfo_timezone_name_get)
	HYSS_FE(timezone_name_from_abbr, arginfo_timezone_name_from_abbr)
	HYSS_FE(timezone_offset_get, arginfo_timezone_offset_get)
	HYSS_FE(timezone_transitions_get, arginfo_timezone_transitions_get)
	HYSS_FE(timezone_location_get, arginfo_timezone_location_get)
	HYSS_FE(timezone_identifiers_list, arginfo_timezone_identifiers_list)
	HYSS_FE(timezone_abbreviations_list, arginfo_timezone_abbreviations_list)
	HYSS_FE(timezone_version_get, arginfo_timezone_version_get)

	HYSS_FE(date_interval_create_from_date_string, arginfo_date_interval_create_from_date_string)
	HYSS_FE(date_interval_format, arginfo_date_interval_format)

	/* Options and Configuration */
	HYSS_FE(date_default_timezone_set, arginfo_date_default_timezone_set)
	HYSS_FE(date_default_timezone_get, arginfo_date_default_timezone_get)

	/* Astronomical functions */
	HYSS_FE(date_sunrise, arginfo_date_sunrise)
	HYSS_FE(date_sunset, arginfo_date_sunset)
	HYSS_FE(date_sun_info, arginfo_date_sun_info)
	HYSS_FE_END
};

static const gear_function_entry date_funcs_interface[] = {
	HYSS_ABSTRACT_ME(DateTimeInterface, format, arginfo_date_method_format)
	HYSS_ABSTRACT_ME(DateTimeInterface, getTimezone, arginfo_date_method_timezone_get)
	HYSS_ABSTRACT_ME(DateTimeInterface, getOffset, arginfo_date_method_offset_get)
	HYSS_ABSTRACT_ME(DateTimeInterface, getTimestamp, arginfo_date_method_timestamp_get)
	HYSS_ABSTRACT_ME(DateTimeInterface, diff, arginfo_date_method_diff)
	HYSS_ABSTRACT_ME(DateTimeInterface, __wakeup, NULL)
	HYSS_FE_END
};

static const gear_function_entry date_funcs_date[] = {
	HYSS_ME(DateTime,			__construct,		arginfo_date_create, GEAR_ACC_CTOR|GEAR_ACC_PUBLIC)
	HYSS_ME(DateTime,			__wakeup,			NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(DateTime,			__set_state,		arginfo_date_set_state, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME(DateTime,			createFromImmutable,	arginfo_date_method_create_from_immutable, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(createFromFormat, date_create_from_format,	arginfo_date_create_from_format, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(getLastErrors, date_get_last_errors,	arginfo_date_get_last_errors, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(format,		date_format,		arginfo_date_method_format, 0)
	HYSS_ME_MAPPING(modify,		date_modify,		arginfo_date_method_modify, 0)
	HYSS_ME_MAPPING(add,			date_add,			arginfo_date_method_add, 0)
	HYSS_ME_MAPPING(sub,			date_sub,			arginfo_date_method_sub, 0)
	HYSS_ME_MAPPING(getTimezone, date_timezone_get,	arginfo_date_method_timezone_get, 0)
	HYSS_ME_MAPPING(setTimezone, date_timezone_set,	arginfo_date_method_timezone_set, 0)
	HYSS_ME_MAPPING(getOffset,	date_offset_get,	arginfo_date_method_offset_get, 0)
	HYSS_ME_MAPPING(setTime,		date_time_set,		arginfo_date_method_time_set, 0)
	HYSS_ME_MAPPING(setDate,		date_date_set,		arginfo_date_method_date_set, 0)
	HYSS_ME_MAPPING(setISODate,	date_isodate_set,	arginfo_date_method_isodate_set, 0)
	HYSS_ME_MAPPING(setTimestamp,	date_timestamp_set, arginfo_date_method_timestamp_set, 0)
	HYSS_ME_MAPPING(getTimestamp,	date_timestamp_get, arginfo_date_method_timestamp_get, 0)
	HYSS_ME_MAPPING(diff,			date_diff, arginfo_date_method_diff, 0)
	HYSS_FE_END
};

static const gear_function_entry date_funcs_immutable[] = {
	HYSS_ME(DateTimeImmutable, __construct,   arginfo_date_create, GEAR_ACC_CTOR|GEAR_ACC_PUBLIC)
	HYSS_ME(DateTime, __wakeup,       NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(DateTimeImmutable, __set_state,   arginfo_date_set_state, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(createFromFormat, date_create_immutable_from_format, arginfo_date_create_from_format, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(getLastErrors,    date_get_last_errors,    arginfo_date_get_last_errors, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(format,           date_format,             arginfo_date_method_format, 0)
	HYSS_ME_MAPPING(getTimezone, date_timezone_get,	arginfo_date_method_timezone_get, 0)
	HYSS_ME_MAPPING(getOffset,	date_offset_get,	arginfo_date_method_offset_get, 0)
	HYSS_ME_MAPPING(getTimestamp,	date_timestamp_get, arginfo_date_method_timestamp_get, 0)
	HYSS_ME_MAPPING(diff,			date_diff, arginfo_date_method_diff, 0)
	HYSS_ME(DateTimeImmutable, modify,        arginfo_date_method_modify, 0)
	HYSS_ME(DateTimeImmutable, add,           arginfo_date_method_add, 0)
	HYSS_ME(DateTimeImmutable, sub,           arginfo_date_method_sub, 0)
	HYSS_ME(DateTimeImmutable, setTimezone,   arginfo_date_method_timezone_set, 0)
	HYSS_ME(DateTimeImmutable, setTime,       arginfo_date_method_time_set, 0)
	HYSS_ME(DateTimeImmutable, setDate,       arginfo_date_method_date_set, 0)
	HYSS_ME(DateTimeImmutable, setISODate,    arginfo_date_method_isodate_set, 0)
	HYSS_ME(DateTimeImmutable, setTimestamp,  arginfo_date_method_timestamp_set, 0)
	HYSS_ME(DateTimeImmutable, createFromMutable, arginfo_date_method_create_from_mutable, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_FE_END
};

static const gear_function_entry date_funcs_timezone[] = {
	HYSS_ME(DateTimeZone,              __construct,                 arginfo_timezone_open, GEAR_ACC_CTOR|GEAR_ACC_PUBLIC)
	HYSS_ME(DateTimeZone,              __wakeup,                    NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(DateTimeZone,              __set_state,                 arginfo_date_set_state, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(getName,           timezone_name_get,           arginfo_timezone_method_name_get, 0)
	HYSS_ME_MAPPING(getOffset,         timezone_offset_get,         arginfo_timezone_method_offset_get, 0)
	HYSS_ME_MAPPING(getTransitions,    timezone_transitions_get,    arginfo_timezone_method_transitions_get, 0)
	HYSS_ME_MAPPING(getLocation,       timezone_location_get,       arginfo_timezone_method_location_get, 0)
	HYSS_ME_MAPPING(listAbbreviations, timezone_abbreviations_list, arginfo_timezone_abbreviations_list, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(listIdentifiers,   timezone_identifiers_list,   arginfo_timezone_identifiers_list, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_FE_END
};

static const gear_function_entry date_funcs_interval[] = {
	HYSS_ME(DateInterval,              __construct,                 arginfo_date_interval_construct, GEAR_ACC_CTOR|GEAR_ACC_PUBLIC)
	HYSS_ME(DateInterval,              __wakeup,                    NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(DateInterval,              __set_state,                 arginfo_date_set_state, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME_MAPPING(format,            date_interval_format,        arginfo_date_method_interval_format, 0)
	HYSS_ME_MAPPING(createFromDateString, date_interval_create_from_date_string,	arginfo_date_interval_create_from_date_string, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_FE_END
};

static const gear_function_entry date_funcs_period[] = {
	HYSS_ME(DatePeriod,                __construct,                 arginfo_date_period_construct, GEAR_ACC_CTOR|GEAR_ACC_PUBLIC)
	HYSS_ME(DatePeriod,                __wakeup,                    NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(DatePeriod,                __set_state,                 arginfo_date_set_state, GEAR_ACC_PUBLIC|GEAR_ACC_STATIC)
	HYSS_ME(DatePeriod,                getStartDate,                NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(DatePeriod,                getEndDate,                  NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(DatePeriod,                getDateInterval,             NULL, GEAR_ACC_PUBLIC)
	HYSS_ME(DatePeriod,                getRecurrences,              NULL, GEAR_ACC_PUBLIC)
	HYSS_FE_END
};

static char* guess_timezone(const timelib_tzdb *tzdb);
static void date_register_classes(void);
/* }}} */

GEAR_DECLARE_CAPI_GLOBALS(date)
static HYSS_GINIT_FUNCTION(date);

/* True global */
timelib_tzdb *hyss_date_global_timezone_db;
int hyss_date_global_timezone_db_enabled;

#define DATE_DEFAULT_LATITUDE "31.7667"
#define DATE_DEFAULT_LONGITUDE "35.2333"

/* on 90'35; common sunset declaration (start of sun body appear) */
#define DATE_SUNSET_ZENITH "90.583333"

/* on 90'35; common sunrise declaration (sun body disappeared) */
#define DATE_SUNRISE_ZENITH "90.583333"

static HYSS_ICS_MH(OnUpdate_date_timezone);

/* {{{ ICS Settings */
HYSS_ICS_BEGIN()
	STD_HYSS_ICS_ENTRY("date.timezone", "", HYSS_ICS_ALL, OnUpdate_date_timezone, default_timezone, gear_date_globals, date_globals)
	HYSS_ICS_ENTRY("date.default_latitude",           DATE_DEFAULT_LATITUDE,        HYSS_ICS_ALL, NULL)
	HYSS_ICS_ENTRY("date.default_longitude",          DATE_DEFAULT_LONGITUDE,       HYSS_ICS_ALL, NULL)
	HYSS_ICS_ENTRY("date.sunset_zenith",              DATE_SUNSET_ZENITH,           HYSS_ICS_ALL, NULL)
	HYSS_ICS_ENTRY("date.sunrise_zenith",             DATE_SUNRISE_ZENITH,          HYSS_ICS_ALL, NULL)
HYSS_ICS_END()
/* }}} */

gear_class_entry *date_ce_date, *date_ce_timezone, *date_ce_interval, *date_ce_period;
gear_class_entry *date_ce_immutable, *date_ce_interface;


HYSSAPI gear_class_entry *hyss_date_get_date_ce(void)
{
	return date_ce_date;
}

HYSSAPI gear_class_entry *hyss_date_get_immutable_ce(void)
{
	return date_ce_immutable;
}

HYSSAPI gear_class_entry *hyss_date_get_interface_ce(void)
{
	return date_ce_interface;
}

HYSSAPI gear_class_entry *hyss_date_get_timezone_ce(void)
{
	return date_ce_timezone;
}

HYSSAPI gear_class_entry *hyss_date_get_interval_ce(void)
{
	return date_ce_interval;
}

HYSSAPI gear_class_entry *hyss_date_get_period_ce(void)
{
	return date_ce_period;
}

static gear_object_handlers date_object_handlers_date;
static gear_object_handlers date_object_handlers_immutable;
static gear_object_handlers date_object_handlers_timezone;
static gear_object_handlers date_object_handlers_interval;
static gear_object_handlers date_object_handlers_period;

#define DATE_SET_CONTEXT \
	zval *object; \
	object = getThis(); \

#define DATE_FETCH_OBJECT	\
	hyss_date_obj *obj;	\
	DATE_SET_CONTEXT; \
	if (object) {	\
		if (gear_parse_parameters_none() == FAILURE) {	\
			return;	\
		}	\
	} else {	\
		if (gear_parse_method_parameters(GEAR_NUM_ARGS(), NULL, "O", &object, date_ce_date) == FAILURE) {	\
			RETURN_FALSE;	\
		}	\
	}	\
	obj = Z_HYSSDATE_P(object);	\

#define DATE_CHECK_INITIALIZED(member, class_name) \
	if (!(member)) { \
		hyss_error_docref(NULL, E_WARNING, "The " #class_name " object has not been correctly initialized by its constructor"); \
		RETURN_FALSE; \
	}

static void date_object_free_storage_date(gear_object *object);
static void date_object_free_storage_timezone(gear_object *object);
static void date_object_free_storage_interval(gear_object *object);
static void date_object_free_storage_period(gear_object *object);

static gear_object *date_object_new_date(gear_class_entry *class_type);
static gear_object *date_object_new_timezone(gear_class_entry *class_type);
static gear_object *date_object_new_interval(gear_class_entry *class_type);
static gear_object *date_object_new_period(gear_class_entry *class_type);

static gear_object *date_object_clone_date(zval *this_ptr);
static gear_object *date_object_clone_timezone(zval *this_ptr);
static gear_object *date_object_clone_interval(zval *this_ptr);
static gear_object *date_object_clone_period(zval *this_ptr);

static int date_object_compare_date(zval *d1, zval *d2);
static HashTable *date_object_get_gc(zval *object, zval **table, int *n);
static HashTable *date_object_get_properties(zval *object);
static HashTable *date_object_get_gc_interval(zval *object, zval **table, int *n);
static HashTable *date_object_get_properties_interval(zval *object);
static HashTable *date_object_get_gc_period(zval *object, zval **table, int *n);
static HashTable *date_object_get_properties_period(zval *object);
static HashTable *date_object_get_properties_timezone(zval *object);
static HashTable *date_object_get_gc_timezone(zval *object, zval **table, int *n);
static HashTable *date_object_get_debug_info_timezone(zval *object, int *is_temp);
static void hyss_timezone_to_string(hyss_timezone_obj *tzobj, zval *zv);

zval *date_interval_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv);
void date_interval_write_property(zval *object, zval *member, zval *value, void **cache_slot);
static zval *date_interval_get_property_ptr_ptr(zval *object, zval *member, int type, void **cache_slot);
static zval *date_period_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv);
static void date_period_write_property(zval *object, zval *member, zval *value, void **cache_slot);

/* {{{ cAPI struct */
gear_capi_entry date_capi_entry = {
	STANDARD_CAPI_HEADER_EX,
	NULL,
	NULL,
	"date",                      /* extension name */
	date_functions,              /* function list */
	HYSS_MINIT(date),            /* process startup */
	HYSS_MSHUTDOWN(date),        /* process shutdown */
	HYSS_RINIT(date),            /* request startup */
	HYSS_RSHUTDOWN(date),        /* request shutdown */
	HYSS_MINFO(date),            /* extension info */
	HYSS_DATE_VERSION,           /* extension version */
	HYSS_CAPI_GLOBALS(date),     /* globals descriptor */
	HYSS_GINIT(date),            /* globals ctor */
	NULL,                        /* globals dtor */
	NULL,                        /* post deactivate */
	STANDARD_CAPI_PROPERTIES_EX
};
/* }}} */


/* {{{ HYSS_GINIT_FUNCTION */
static HYSS_GINIT_FUNCTION(date)
{
	date_globals->default_timezone = NULL;
	date_globals->timezone = NULL;
	date_globals->tzcache = NULL;
	date_globals->timezone_valid = 0;
}
/* }}} */


static void _hyss_date_tzinfo_dtor(zval *zv) /* {{{ */
{
	timelib_tzinfo *tzi = (timelib_tzinfo*)Z_PTR_P(zv);

	timelib_tzinfo_dtor(tzi);
} /* }}} */

/* {{{ HYSS_RINIT_FUNCTION */
HYSS_RINIT_FUNCTION(date)
{
	if (DATEG(timezone)) {
		efree(DATEG(timezone));
	}
	DATEG(timezone) = NULL;
	DATEG(tzcache) = NULL;
	DATEG(last_errors) = NULL;

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_RSHUTDOWN_FUNCTION */
HYSS_RSHUTDOWN_FUNCTION(date)
{
	if (DATEG(timezone)) {
		efree(DATEG(timezone));
	}
	DATEG(timezone) = NULL;
	if(DATEG(tzcache)) {
		gear_hash_destroy(DATEG(tzcache));
		FREE_HASHTABLE(DATEG(tzcache));
		DATEG(tzcache) = NULL;
	}
	if (DATEG(last_errors)) {
		timelib_error_container_dtor(DATEG(last_errors));
		DATEG(last_errors) = NULL;
	}

	return SUCCESS;
}
/* }}} */

#define DATE_TIMEZONEDB      hyss_date_global_timezone_db ? hyss_date_global_timezone_db : timelib_builtin_db()

/*
 * RFC822, Section 5.1: http://www.ietf.org/rfc/rfc822.txt
 *  date-time   =  [ day "," ] date time        ; dd mm yy hh:mm:ss zzz
 *  day         =  "Mon"  / "Tue" /  "Wed"  / "Thu"  /  "Fri"  / "Sat" /  "Sun"
 *  date        =  1*2DIGIT month 2DIGIT        ; day month year e.g. 20 Jun 82
 *  month       =  "Jan"  /  "Feb" /  "Mar"  /  "Apr"  /  "May"  /  "Jun" /  "Jul"  /  "Aug"  /  "Sep"  /  "Oct" /  "Nov"  /  "Dec"
 *  time        =  hour zone                    ; ANSI and Military
 *  hour        =  2DIGIT ":" 2DIGIT [":" 2DIGIT] ; 00:00:00 - 23:59:59
 *  zone        =  "UT"  / "GMT"  /  "EST" / "EDT"  /  "CST" / "CDT"  /  "MST" / "MDT"  /  "PST" / "PDT"  /  1ALPHA  / ( ("+" / "-") 4DIGIT )
 */
#define DATE_FORMAT_RFC822   "D, d M y H:i:s O"

/*
 * RFC850, Section 2.1.4: http://www.ietf.org/rfc/rfc850.txt
 *  Format must be acceptable both to the ARPANET and to the getdate routine.
 *  One format that is acceptable to both is Weekday, DD-Mon-YY HH:MM:SS TIMEZONE
 *  TIMEZONE can be any timezone name (3 or more letters)
 */
#define DATE_FORMAT_RFC850   "l, d-M-y H:i:s T"

/*
 * RFC1036, Section 2.1.2: http://www.ietf.org/rfc/rfc1036.txt
 *  Its format must be acceptable both in RFC-822 and to the getdate(3)
 *  Wdy, DD Mon YY HH:MM:SS TIMEZONE
 *  There is no hope of having a complete list of timezones.  Universal
 *  Time (GMT), the North American timezones (PST, PDT, MST, MDT, CST,
 *  CDT, EST, EDT) and the +/-hhmm offset specifed in RFC-822 should be supported.
 */
#define DATE_FORMAT_RFC1036  "D, d M y H:i:s O"

/*
 * RFC1123, Section 5.2.14: http://www.ietf.org/rfc/rfc1123.txt
 *  RFC-822 Date and Time Specification: RFC-822 Section 5
 *  The syntax for the date is hereby changed to: date = 1*2DIGIT month 2*4DIGIT
 */
#define DATE_FORMAT_RFC1123  "D, d M Y H:i:s O"

/*
 * RFC7231, Section 7.1.1: http://tools.ietf.org/html/rfc7231
 */
#define DATE_FORMAT_RFC7231  "D, d M Y H:i:s \\G\\M\\T"

/*
 * RFC2822, Section 3.3: http://www.ietf.org/rfc/rfc2822.txt
 *  FWS             =       ([*WSP CRLF] 1*WSP) /   ; Folding white space
 *  CFWS            =       *([FWS] comment) (([FWS] comment) / FWS)
 *
 *  date-time       =       [ day-of-week "," ] date FWS time [CFWS]
 *  day-of-week     =       ([FWS] day-name)
 *  day-name        =       "Mon" / "Tue" / "Wed" / "Thu" / "Fri" / "Sat" / "Sun"
 *  date            =       day month year
 *  year            =       4*DIGIT
 *  month           =       (FWS month-name FWS)
 *  month-name      =       "Jan" / "Feb" / "Mar" / "Apr" / "May" / "Jun" / "Jul" / "Aug" / "Sep" / "Oct" / "Nov" / "Dec"
 *  day             =       ([FWS] 1*2DIGIT)
 *  time            =       time-of-day FWS zone
 *  time-of-day     =       hour ":" minute [ ":" second ]
 *  hour            =       2DIGIT
 *  minute          =       2DIGIT
 *  second          =       2DIGIT
 *  zone            =       (( "+" / "-" ) 4DIGIT)
 */
#define DATE_FORMAT_RFC2822  "D, d M Y H:i:s O"
/*
 * RFC3339, Section 5.6: http://www.ietf.org/rfc/rfc3339.txt
 *  date-fullyear   = 4DIGIT
 *  date-month      = 2DIGIT  ; 01-12
 *  date-mday       = 2DIGIT  ; 01-28, 01-29, 01-30, 01-31 based on month/year
 *
 *  time-hour       = 2DIGIT  ; 00-23
 *  time-minute     = 2DIGIT  ; 00-59
 *  time-second     = 2DIGIT  ; 00-58, 00-59, 00-60 based on leap second rules
 *
 *  time-secfrac    = "." 1*DIGIT
 *  time-numoffset  = ("+" / "-") time-hour ":" time-minute
 *  time-offset     = "Z" / time-numoffset
 *
 *  partial-time    = time-hour ":" time-minute ":" time-second [time-secfrac]
 *  full-date       = date-fullyear "-" date-month "-" date-mday
 *  full-time       = partial-time time-offset
 *
 *  date-time       = full-date "T" full-time
 */
#define DATE_FORMAT_RFC3339  "Y-m-d\\TH:i:sP"

#define DATE_FORMAT_ISO8601  "Y-m-d\\TH:i:sO"

/*
 * RFC3339, Appendix A: http://www.ietf.org/rfc/rfc3339.txt
 *  ISO 8601 also requires (in section 5.3.1.3) that a decimal fraction
 *  be proceeded by a "0" if less than unity.  Annex B.2 of ISO 8601
 *  gives examples where the decimal fractions are not preceded by a "0".
 *  This grammar assumes section 5.3.1.3 is correct and that Annex B.2 is
 *  in error.
 */
#define DATE_FORMAT_RFC3339_EXTENDED  "Y-m-d\\TH:i:s.vP"

/*
 * This comes from various sources that like to contradict. I'm going with the
 * format here because of:
 * http://msdn.microsoft.com/en-us/library/windows/desktop/aa384321%28v=vs.85%29.aspx
 * and http://curl.haxx.se/rfc/cookie_spec.html
 */
#define DATE_FORMAT_COOKIE   "l, d-M-Y H:i:s T"

#define SUNFUNCS_RET_TIMESTAMP 0
#define SUNFUNCS_RET_STRING    1
#define SUNFUNCS_RET_DOUBLE    2

/* {{{ HYSS_MINIT_FUNCTION */
HYSS_MINIT_FUNCTION(date)
{
	REGISTER_ICS_ENTRIES();
	date_register_classes();
/*
 * RFC4287, Section 3.3: http://www.ietf.org/rfc/rfc4287.txt
 *   A Date construct is an element whose content MUST conform to the
 *   "date-time" production in [RFC3339].  In addition, an uppercase "T"
 *   character MUST be used to separate date and time, and an uppercase
 *   "Z" character MUST be present in the absence of a numeric time zone offset.
 */
	REGISTER_STRING_CONSTANT("DATE_ATOM",    DATE_FORMAT_RFC3339, CONST_CS | CONST_PERSISTENT);
/*
 * Preliminary specification: http://wp.netscape.com/newsref/std/cookie_spec.html
 *   "This is based on RFC 822, RFC 850,  RFC 1036, and  RFC 1123,
 *   with the variations that the only legal time zone is GMT
 *   and the separators between the elements of the date must be dashes."
 */
	REGISTER_STRING_CONSTANT("DATE_COOKIE",  DATE_FORMAT_COOKIE,  CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_ISO8601", DATE_FORMAT_ISO8601, CONST_CS | CONST_PERSISTENT);

	REGISTER_STRING_CONSTANT("DATE_RFC822",  DATE_FORMAT_RFC822,  CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_RFC850",  DATE_FORMAT_RFC850,  CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_RFC1036", DATE_FORMAT_RFC1036, CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_RFC1123", DATE_FORMAT_RFC1123, CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_RFC7231", DATE_FORMAT_RFC7231, CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_RFC2822", DATE_FORMAT_RFC2822, CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_RFC3339", DATE_FORMAT_RFC3339, CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_RFC3339_EXTENDED", DATE_FORMAT_RFC3339_EXTENDED, CONST_CS | CONST_PERSISTENT);

/*
 * RSS 2.0 Specification: http://blogs.law.harvard.edu/tech/rss
 *   "All date-times in RSS conform to the Date and Time Specification of RFC 822,
 *   with the exception that the year may be expressed with two characters or four characters (four preferred)"
 */
	REGISTER_STRING_CONSTANT("DATE_RSS",     DATE_FORMAT_RFC1123, CONST_CS | CONST_PERSISTENT);
	REGISTER_STRING_CONSTANT("DATE_W3C",     DATE_FORMAT_RFC3339, CONST_CS | CONST_PERSISTENT);

	REGISTER_LONG_CONSTANT("SUNFUNCS_RET_TIMESTAMP", SUNFUNCS_RET_TIMESTAMP, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("SUNFUNCS_RET_STRING", SUNFUNCS_RET_STRING, CONST_CS | CONST_PERSISTENT);
	REGISTER_LONG_CONSTANT("SUNFUNCS_RET_DOUBLE", SUNFUNCS_RET_DOUBLE, CONST_CS | CONST_PERSISTENT);

	hyss_date_global_timezone_db = NULL;
	hyss_date_global_timezone_db_enabled = 0;
	DATEG(last_errors) = NULL;
	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MSHUTDOWN_FUNCTION */
HYSS_MSHUTDOWN_FUNCTION(date)
{
	UNREGISTER_ICS_ENTRIES();

	if (DATEG(last_errors)) {
		timelib_error_container_dtor(DATEG(last_errors));
	}

#ifndef ZTS
	DATEG(default_timezone) = NULL;
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ HYSS_MINFO_FUNCTION */
HYSS_MINFO_FUNCTION(date)
{
	const timelib_tzdb *tzdb = DATE_TIMEZONEDB;

	hyss_info_print_table_start();
	hyss_info_print_table_row(2, "date/time support", "enabled");
	hyss_info_print_table_row(2, "timelib version", TIMELIB_ASCII_VERSION);
	hyss_info_print_table_row(2, "\"Olson\" Timezone Database Version", tzdb->version);
	hyss_info_print_table_row(2, "Timezone Database", hyss_date_global_timezone_db_enabled ? "external" : "internal");
	hyss_info_print_table_row(2, "Default timezone", guess_timezone(tzdb));
	hyss_info_print_table_end();

	DISPLAY_ICS_ENTRIES();
}
/* }}} */

/* {{{ Timezone Cache functions */
static timelib_tzinfo *hyss_date_parse_tzfile(char *formal_tzname, const timelib_tzdb *tzdb)
{
	timelib_tzinfo *tzi;
	int dummy_error_code;

	if(!DATEG(tzcache)) {
		ALLOC_HASHTABLE(DATEG(tzcache));
		gear_hash_init(DATEG(tzcache), 4, NULL, _hyss_date_tzinfo_dtor, 0);
	}

	if ((tzi = gear_hash_str_find_ptr(DATEG(tzcache), formal_tzname, strlen(formal_tzname))) != NULL) {
		return tzi;
	}

	tzi = timelib_parse_tzfile(formal_tzname, tzdb, &dummy_error_code);
	if (tzi) {
		gear_hash_str_add_ptr(DATEG(tzcache), formal_tzname, strlen(formal_tzname), tzi);
	}
	return tzi;
}

timelib_tzinfo *hyss_date_parse_tzfile_wrapper(char *formal_tzname, const timelib_tzdb *tzdb, int *dummy_error_code)
{
	return hyss_date_parse_tzfile(formal_tzname, tzdb);
}
/* }}} */

/* Callback to check the date.timezone only when changed increases performance */
/* {{{ static HYSS_ICS_MH(OnUpdate_date_timezone) */
static HYSS_ICS_MH(OnUpdate_date_timezone)
{
	if (OnUpdateString(entry, new_value, mh_arg1, mh_arg2, mh_arg3, stage) == FAILURE) {
		return FAILURE;
	}

	DATEG(timezone_valid) = 0;
	if (stage == HYSS_ICS_STAGE_RUNTIME) {
		if (!timelib_timezone_id_is_valid(DATEG(default_timezone), DATE_TIMEZONEDB)) {
			if (DATEG(default_timezone) && *DATEG(default_timezone)) {
				hyss_error_docref(NULL, E_WARNING, "Invalid date.timezone value '%s', we selected the timezone 'UTC' for now.", DATEG(default_timezone));
			}
		} else {
			DATEG(timezone_valid) = 1;
		}
	}

	return SUCCESS;
}
/* }}} */

/* {{{ Helper functions */
static char* guess_timezone(const timelib_tzdb *tzdb)
{
	/* Checking configure timezone */
	if (DATEG(timezone) && (strlen(DATEG(timezone))) > 0) {
		return DATEG(timezone);
	}
	/* Check config setting for default timezone */
	if (!DATEG(default_timezone)) {
		/* Special case: extslib/date wasn't initialized yet */
		zval *ztz;

		if (NULL != (ztz = cfg_get_entry("date.timezone", sizeof("date.timezone")))
			&& Z_TYPE_P(ztz) == IS_STRING && Z_STRLEN_P(ztz) > 0 && timelib_timezone_id_is_valid(Z_STRVAL_P(ztz), tzdb)) {
			return Z_STRVAL_P(ztz);
		}
	} else if (*DATEG(default_timezone)) {
		if (DATEG(timezone_valid) == 1) {
			return DATEG(default_timezone);
		}

		if (!timelib_timezone_id_is_valid(DATEG(default_timezone), tzdb)) {
			hyss_error_docref(NULL, E_WARNING, "Invalid date.timezone value '%s', we selected the timezone 'UTC' for now.", DATEG(default_timezone));
			return "UTC";
		}

		DATEG(timezone_valid) = 1;
		return DATEG(default_timezone);
	}
	/* Fallback to UTC */
	return "UTC";
}

HYSSAPI timelib_tzinfo *get_timezone_info(void)
{
	char *tz;
	timelib_tzinfo *tzi;

	tz = guess_timezone(DATE_TIMEZONEDB);
	tzi = hyss_date_parse_tzfile(tz, DATE_TIMEZONEDB);
	if (!tzi) {
		hyss_error_docref(NULL, E_ERROR, "Timezone database is corrupt - this should *never* happen!");
	}
	return tzi;
}
/* }}} */


/* {{{ date() and gmdate() data */
#include "gear_smart_str.h"

static const char * const mon_full_names[] = {
	"January", "February", "March", "April",
	"May", "June", "July", "August",
	"September", "October", "November", "December"
};

static const char * const mon_short_names[] = {
	"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

static const char * const day_full_names[] = {
	"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
};

static const char * const day_short_names[] = {
	"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
};

static char *english_suffix(timelib_sll number)
{
	if (number >= 10 && number <= 19) {
		return "th";
	} else {
		switch (number % 10) {
			case 1: return "st";
			case 2: return "nd";
			case 3: return "rd";
		}
	}
	return "th";
}
/* }}} */

/* {{{ day of week helpers */
static const char *hyss_date_full_day_name(timelib_sll y, timelib_sll m, timelib_sll d)
{
	timelib_sll day_of_week = timelib_day_of_week(y, m, d);
	if (day_of_week < 0) {
		return "Unknown";
	}
	return day_full_names[day_of_week];
}

static const char *hyss_date_short_day_name(timelib_sll y, timelib_sll m, timelib_sll d)
{
	timelib_sll day_of_week = timelib_day_of_week(y, m, d);
	if (day_of_week < 0) {
		return "Unknown";
	}
	return day_short_names[day_of_week];
}
/* }}} */

/* {{{ date_format - (gm)date helper */
static gear_string *date_format(char *format, size_t format_len, timelib_time *t, int localtime)
{
	smart_str            string = {0};
	size_t               i;
	int                  length = 0;
	char                 buffer[97];
	timelib_time_offset *offset = NULL;
	timelib_sll          isoweek, isoyear;
	int                  rfc_colon;
	int                  weekYearSet = 0;

	if (!format_len) {
		return ZSTR_EMPTY_ALLOC();
	}

	if (localtime) {
		if (t->zone_type == TIMELIB_ZONETYPE_ABBR) {
			offset = timelib_time_offset_ctor();
			offset->offset = (t->z + (t->dst * 3600));
			offset->leap_secs = 0;
			offset->is_dst = t->dst;
			offset->abbr = timelib_strdup(t->tz_abbr);
		} else if (t->zone_type == TIMELIB_ZONETYPE_OFFSET) {
			offset = timelib_time_offset_ctor();
			offset->offset = (t->z);
			offset->leap_secs = 0;
			offset->is_dst = 0;
			offset->abbr = timelib_malloc(9); /* GMT±xxxx\0 */
			snprintf(offset->abbr, 9, "GMT%c%02d%02d",
			                          (offset->offset < 0) ? '-' : '+',
			                          abs(offset->offset / 3600),
			                          abs((offset->offset % 3600) / 60));
		} else {
			offset = timelib_get_time_zone_info(t->sse, t->tz_info);
		}
	}

	for (i = 0; i < format_len; i++) {
		rfc_colon = 0;
		switch (format[i]) {
			/* day */
			case 'd': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->d); break;
			case 'D': length = slprintf(buffer, sizeof(buffer), "%s", hyss_date_short_day_name(t->y, t->m, t->d)); break;
			case 'j': length = slprintf(buffer, sizeof(buffer), "%d", (int) t->d); break;
			case 'l': length = slprintf(buffer, sizeof(buffer), "%s", hyss_date_full_day_name(t->y, t->m, t->d)); break;
			case 'S': length = slprintf(buffer, sizeof(buffer), "%s", english_suffix(t->d)); break;
			case 'w': length = slprintf(buffer, sizeof(buffer), "%d", (int) timelib_day_of_week(t->y, t->m, t->d)); break;
			case 'N': length = slprintf(buffer, sizeof(buffer), "%d", (int) timelib_iso_day_of_week(t->y, t->m, t->d)); break;
			case 'z': length = slprintf(buffer, sizeof(buffer), "%d", (int) timelib_day_of_year(t->y, t->m, t->d)); break;

			/* week */
			case 'W':
				if(!weekYearSet) { timelib_isoweek_from_date(t->y, t->m, t->d, &isoweek, &isoyear); weekYearSet = 1; }
				length = slprintf(buffer, sizeof(buffer), "%02d", (int) isoweek); break; /* iso weeknr */
			case 'o':
				if(!weekYearSet) { timelib_isoweek_from_date(t->y, t->m, t->d, &isoweek, &isoyear); weekYearSet = 1; }
				length = slprintf(buffer, sizeof(buffer), GEAR_LONG_FMT, (gear_long) isoyear); break; /* iso year */

			/* month */
			case 'F': length = slprintf(buffer, sizeof(buffer), "%s", mon_full_names[t->m - 1]); break;
			case 'm': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->m); break;
			case 'M': length = slprintf(buffer, sizeof(buffer), "%s", mon_short_names[t->m - 1]); break;
			case 'n': length = slprintf(buffer, sizeof(buffer), "%d", (int) t->m); break;
			case 't': length = slprintf(buffer, sizeof(buffer), "%d", (int) timelib_days_in_month(t->y, t->m)); break;

			/* year */
			case 'L': length = slprintf(buffer, sizeof(buffer), "%d", timelib_is_leap((int) t->y)); break;
			case 'y': length = slprintf(buffer, sizeof(buffer), "%02d", (int) (t->y % 100)); break;
			case 'Y': length = slprintf(buffer, sizeof(buffer), "%s%04lld", t->y < 0 ? "-" : "", hyss_date_llabs((timelib_sll) t->y)); break;

			/* time */
			case 'a': length = slprintf(buffer, sizeof(buffer), "%s", t->h >= 12 ? "pm" : "am"); break;
			case 'A': length = slprintf(buffer, sizeof(buffer), "%s", t->h >= 12 ? "PM" : "AM"); break;
			case 'B': {
				int retval = ((((long)t->sse)-(((long)t->sse) - ((((long)t->sse) % 86400) + 3600))) * 10);
				if (retval < 0) {
					retval += 864000;
				}
				/* Make sure to do this on a positive int to avoid rounding errors */
				retval = (retval / 864)  % 1000;
				length = slprintf(buffer, sizeof(buffer), "%03d", retval);
				break;
			}
			case 'g': length = slprintf(buffer, sizeof(buffer), "%d", (t->h % 12) ? (int) t->h % 12 : 12); break;
			case 'G': length = slprintf(buffer, sizeof(buffer), "%d", (int) t->h); break;
			case 'h': length = slprintf(buffer, sizeof(buffer), "%02d", (t->h % 12) ? (int) t->h % 12 : 12); break;
			case 'H': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->h); break;
			case 'i': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->i); break;
			case 's': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->s); break;
			case 'u': length = slprintf(buffer, sizeof(buffer), "%06d", (int) floor(t->us)); break;
			case 'v': length = slprintf(buffer, sizeof(buffer), "%03d", (int) floor(t->us / 1000)); break;

			/* timezone */
			case 'I': length = slprintf(buffer, sizeof(buffer), "%d", localtime ? offset->is_dst : 0); break;
			case 'P': rfc_colon = 1; /* break intentionally missing */
			case 'O': length = slprintf(buffer, sizeof(buffer), "%c%02d%s%02d",
											localtime ? ((offset->offset < 0) ? '-' : '+') : '+',
											localtime ? abs(offset->offset / 3600) : 0,
											rfc_colon ? ":" : "",
											localtime ? abs((offset->offset % 3600) / 60) : 0
							  );
					  break;
			case 'T': length = slprintf(buffer, sizeof(buffer), "%s", localtime ? offset->abbr : "GMT"); break;
			case 'e': if (!localtime) {
					      length = slprintf(buffer, sizeof(buffer), "%s", "UTC");
					  } else {
						  switch (t->zone_type) {
							  case TIMELIB_ZONETYPE_ID:
								  length = slprintf(buffer, sizeof(buffer), "%s", t->tz_info->name);
								  break;
							  case TIMELIB_ZONETYPE_ABBR:
								  length = slprintf(buffer, sizeof(buffer), "%s", offset->abbr);
								  break;
							  case TIMELIB_ZONETYPE_OFFSET:
								  length = slprintf(buffer, sizeof(buffer), "%c%02d:%02d",
												((offset->offset < 0) ? '-' : '+'),
												abs(offset->offset / 3600),
												abs((offset->offset % 3600) / 60)
										   );
								  break;
						  }
					  }
					  break;
			case 'Z': length = slprintf(buffer, sizeof(buffer), "%d", localtime ? offset->offset : 0); break;

			/* full date/time */
			case 'c': length = slprintf(buffer, sizeof(buffer), "%04" GEAR_LONG_FMT_SPEC "-%02d-%02dT%02d:%02d:%02d%c%02d:%02d",
							                (gear_long) t->y, (int) t->m, (int) t->d,
											(int) t->h, (int) t->i, (int) t->s,
											localtime ? ((offset->offset < 0) ? '-' : '+') : '+',
											localtime ? abs(offset->offset / 3600) : 0,
											localtime ? abs((offset->offset % 3600) / 60) : 0
							  );
					  break;
			case 'r': length = slprintf(buffer, sizeof(buffer), "%3s, %02d %3s %04" GEAR_LONG_FMT_SPEC " %02d:%02d:%02d %c%02d%02d",
							                hyss_date_short_day_name(t->y, t->m, t->d),
											(int) t->d, mon_short_names[t->m - 1],
											(gear_long) t->y, (int) t->h, (int) t->i, (int) t->s,
											localtime ? ((offset->offset < 0) ? '-' : '+') : '+',
											localtime ? abs(offset->offset / 3600) : 0,
											localtime ? abs((offset->offset % 3600) / 60) : 0
							  );
					  break;
			case 'U': length = slprintf(buffer, sizeof(buffer), "%lld", (timelib_sll) t->sse); break;

			case '\\': if (i < format_len) i++; /* break intentionally missing */

			default: buffer[0] = format[i]; buffer[1] = '\0'; length = 1; break;
		}
		smart_str_appendl(&string, buffer, length);
	}

	smart_str_0(&string);

	if (localtime) {
		timelib_time_offset_dtor(offset);
	}

	return string.s;
}

static void hyss_date(INTERNAL_FUNCTION_PARAMETERS, int localtime)
{
	gear_string *format;
	gear_long    ts;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(format)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(ts)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (GEAR_NUM_ARGS() == 1) {
		ts = time(NULL);
	}

	RETURN_STR(hyss_format_date(ZSTR_VAL(format), ZSTR_LEN(format), ts, localtime));
}
/* }}} */

HYSSAPI gear_string *hyss_format_date(char *format, size_t format_len, time_t ts, int localtime) /* {{{ */
{
	timelib_time   *t;
	timelib_tzinfo *tzi;
	gear_string *string;

	t = timelib_time_ctor();

	if (localtime) {
		tzi = get_timezone_info();
		t->tz_info = tzi;
		t->zone_type = TIMELIB_ZONETYPE_ID;
		timelib_unixtime2local(t, ts);
	} else {
		tzi = NULL;
		timelib_unixtime2gmt(t, ts);
	}

	string = date_format(format, format_len, t, localtime);

	timelib_time_dtor(t);
	return string;
}
/* }}} */

/* {{{ hyss_idate
 */
HYSSAPI int hyss_idate(char format, time_t ts, int localtime)
{
	timelib_time   *t;
	timelib_tzinfo *tzi;
	int retval = -1;
	timelib_time_offset *offset = NULL;
	timelib_sll isoweek, isoyear;

	t = timelib_time_ctor();

	if (!localtime) {
		tzi = get_timezone_info();
		t->tz_info = tzi;
		t->zone_type = TIMELIB_ZONETYPE_ID;
		timelib_unixtime2local(t, ts);
	} else {
		tzi = NULL;
		timelib_unixtime2gmt(t, ts);
	}

	if (!localtime) {
		if (t->zone_type == TIMELIB_ZONETYPE_ABBR) {
			offset = timelib_time_offset_ctor();
			offset->offset = (t->z + (t->dst * 3600));
			offset->leap_secs = 0;
			offset->is_dst = t->dst;
			offset->abbr = timelib_strdup(t->tz_abbr);
		} else if (t->zone_type == TIMELIB_ZONETYPE_OFFSET) {
			offset = timelib_time_offset_ctor();
			offset->offset = (t->z + (t->dst * 3600));
			offset->leap_secs = 0;
			offset->is_dst = t->dst;
			offset->abbr = timelib_malloc(9); /* GMT±xxxx\0 */
			snprintf(offset->abbr, 9, "GMT%c%02d%02d",
			                          (offset->offset < 0) ? '-' : '+',
			                          abs(offset->offset / 3600),
			                          abs((offset->offset % 3600) / 60));
		} else {
			offset = timelib_get_time_zone_info(t->sse, t->tz_info);
		}
	}

	timelib_isoweek_from_date(t->y, t->m, t->d, &isoweek, &isoyear);

	switch (format) {
		/* day */
		case 'd': case 'j': retval = (int) t->d; break;

		case 'w': retval = (int) timelib_day_of_week(t->y, t->m, t->d); break;
		case 'z': retval = (int) timelib_day_of_year(t->y, t->m, t->d); break;

		/* week */
		case 'W': retval = (int) isoweek; break; /* iso weeknr */

		/* month */
		case 'm': case 'n': retval = (int) t->m; break;
		case 't': retval = (int) timelib_days_in_month(t->y, t->m); break;

		/* year */
		case 'L': retval = (int) timelib_is_leap((int) t->y); break;
		case 'y': retval = (int) (t->y % 100); break;
		case 'Y': retval = (int) t->y; break;

		/* Swatch Beat a.k.a. Internet Time */
		case 'B':
			retval = ((((long)t->sse)-(((long)t->sse) - ((((long)t->sse) % 86400) + 3600))) * 10);
			if (retval < 0) {
				retval += 864000;
			}
			/* Make sure to do this on a positive int to avoid rounding errors */
			retval = (retval / 864) % 1000;
			break;

		/* time */
		case 'g': case 'h': retval = (int) ((t->h % 12) ? (int) t->h % 12 : 12); break;
		case 'H': case 'G': retval = (int) t->h; break;
		case 'i': retval = (int) t->i; break;
		case 's': retval = (int) t->s; break;

		/* timezone */
		case 'I': retval = (int) (!localtime ? offset->is_dst : 0); break;
		case 'Z': retval = (int) (!localtime ? offset->offset : 0); break;

		case 'U': retval = (int) t->sse; break;
	}

	if (!localtime) {
		timelib_time_offset_dtor(offset);
	}
	timelib_time_dtor(t);

	return retval;
}
/* }}} */

/* {{{ proto string date(string format [, int timestamp])
   Format a local date/time (guessed with bundled builtin timezonedb)*/
HYSS_FUNCTION(date)
{
	hyss_date(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto string gmdate(string format [, int timestamp])
   Format a GMT date/time */
HYSS_FUNCTION(gmdate)
{
	hyss_date(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto int idate(string format [, int timestamp])
   Format a local time/date as integer */
HYSS_FUNCTION(idate)
{
	gear_string *format;
	gear_long    ts = 0;
	int ret;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(format)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(ts)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (ZSTR_LEN(format) != 1) {
		hyss_error_docref(NULL, E_WARNING, "idate format is one char");
		RETURN_FALSE;
	}

	if (GEAR_NUM_ARGS() == 1) {
		ts = time(NULL);
	}

	ret = hyss_idate(ZSTR_VAL(format)[0], ts, 0);
	if (ret == -1) {
		hyss_error_docref(NULL, E_WARNING, "Unrecognized date format token.");
		RETURN_FALSE;
	}
	RETURN_LONG(ret);
}
/* }}} */

/* {{{ hyss_date_set_tzdb - NOT THREADSAFE */
HYSSAPI void hyss_date_set_tzdb(timelib_tzdb *tzdb)
{
	const timelib_tzdb *builtin = timelib_builtin_db();

	if (hyss_version_compare(tzdb->version, builtin->version) > 0) {
		hyss_date_global_timezone_db = tzdb;
		hyss_date_global_timezone_db_enabled = 1;
	}
}
/* }}} */

/* {{{ hyss_parse_date: Backwards compatibility function */
HYSSAPI gear_long hyss_parse_date(char *string, gear_long *now)
{
	timelib_time *parsed_time;
	timelib_error_container *error = NULL;
	int           error2;
	gear_long   retval;

	parsed_time = timelib_strtotime(string, strlen(string), &error, DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);
	if (error->error_count) {
		timelib_time_dtor(parsed_time);
		timelib_error_container_dtor(error);
		return -1;
	}
	timelib_error_container_dtor(error);
	timelib_update_ts(parsed_time, NULL);
	retval = timelib_date_to_int(parsed_time, &error2);
	timelib_time_dtor(parsed_time);
	if (error2) {
		return -1;
	}
	return retval;
}
/* }}} */

/* {{{ proto int strtotime(string time [, int now ])
   Convert string representation of date and time to a timestamp */
HYSS_FUNCTION(strtotime)
{
	gear_string *times;
	int error1, error2;
	timelib_error_container *error;
	gear_long preset_ts = 0, ts;
	timelib_time *t, *now;
	timelib_tzinfo *tzi;

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(times)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(preset_ts)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	tzi = get_timezone_info();

	now = timelib_time_ctor();
	now->tz_info = tzi;
	now->zone_type = TIMELIB_ZONETYPE_ID;
	timelib_unixtime2local(now,
		(GEAR_NUM_ARGS() == 2) ? (timelib_sll) preset_ts : (timelib_sll) time(NULL));

	t = timelib_strtotime(ZSTR_VAL(times), ZSTR_LEN(times), &error,
		DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);
	error1 = error->error_count;
	timelib_error_container_dtor(error);
	timelib_fill_holes(t, now, TIMELIB_NO_CLONE);
	timelib_update_ts(t, tzi);
	ts = timelib_date_to_int(t, &error2);

	timelib_time_dtor(now);
	timelib_time_dtor(t);

	if (error1 || error2) {
		RETURN_FALSE;
	} else {
		RETURN_LONG(ts);
	}
}
/* }}} */

/* {{{ hyss_mktime - (gm)mktime helper */
HYSSAPI void hyss_mktime(INTERNAL_FUNCTION_PARAMETERS, int gmt)
{
	gear_long hou = 0, min = 0, sec = 0, mon = 0, day = 0, yea = 0;
	timelib_time *now;
	timelib_tzinfo *tzi = NULL;
	gear_long ts, adjust_seconds = 0;
	int error;

	GEAR_PARSE_PARAMETERS_START(0, 6)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(hou)
		Z_PARAM_LONG(min)
		Z_PARAM_LONG(sec)
		Z_PARAM_LONG(mon)
		Z_PARAM_LONG(day)
		Z_PARAM_LONG(yea)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	/* Initialize structure with current time */
	now = timelib_time_ctor();
	if (gmt) {
		timelib_unixtime2gmt(now, (timelib_sll) time(NULL));
	} else {
		tzi = get_timezone_info();
		now->tz_info = tzi;
		now->zone_type = TIMELIB_ZONETYPE_ID;
		timelib_unixtime2local(now, (timelib_sll) time(NULL));
	}
	/* Fill in the new data */
	switch (GEAR_NUM_ARGS()) {
		case 7:
			/* break intentionally missing */
		case 6:
			if (yea >= 0 && yea < 70) {
				yea += 2000;
			} else if (yea >= 70 && yea <= 100) {
				yea += 1900;
			}
			now->y = yea;
			/* break intentionally missing again */
		case 5:
			now->d = day;
			/* break missing intentionally here too */
		case 4:
			now->m = mon;
			/* and here */
		case 3:
			now->s = sec;
			/* yup, this break isn't here on purpose too */
		case 2:
			now->i = min;
			/* last intentionally missing break */
		case 1:
			now->h = hou;
			break;
		default:
			hyss_error_docref(NULL, E_DEPRECATED, "You should be using the time() function instead");
	}
	/* Update the timestamp */
	if (gmt) {
		timelib_update_ts(now, NULL);
	} else {
		timelib_update_ts(now, tzi);
	}

	/* Clean up and return */
	ts = timelib_date_to_int(now, &error);
	ts += adjust_seconds;
	timelib_time_dtor(now);

	if (error) {
		RETURN_FALSE;
	} else {
		RETURN_LONG(ts);
	}
}
/* }}} */

/* {{{ proto int mktime([int hour [, int min [, int sec [, int mon [, int day [, int year]]]]]])
   Get UNIX timestamp for a date */
HYSS_FUNCTION(mktime)
{
	hyss_mktime(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto int gmmktime([int hour [, int min [, int sec [, int mon [, int day [, int year]]]]]])
   Get UNIX timestamp for a GMT date */
HYSS_FUNCTION(gmmktime)
{
	hyss_mktime(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto bool checkdate(int month, int day, int year)
   Returns true(1) if it is a valid date in gregorian calendar */
HYSS_FUNCTION(checkdate)
{
	gear_long m, d, y;

	GEAR_PARSE_PARAMETERS_START(3, 3)
		Z_PARAM_LONG(m)
		Z_PARAM_LONG(d)
		Z_PARAM_LONG(y)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (y < 1 || y > 32767 || !timelib_valid_date(y, m, d)) {
		RETURN_FALSE;
	}
	RETURN_TRUE;	/* True : This month, day, year arguments are valid */
}
/* }}} */

#ifdef HAVE_STRFTIME
/* {{{ hyss_strftime - (gm)strftime helper */
HYSSAPI void hyss_strftime(INTERNAL_FUNCTION_PARAMETERS, int gmt)
{
	gear_string         *format;
	gear_long            timestamp = 0;
	struct tm            ta;
	int                  max_reallocs = 5;
	size_t               buf_len = 256, real_len;
	timelib_time        *ts;
	timelib_tzinfo      *tzi;
	timelib_time_offset *offset = NULL;
	gear_string 		*buf;

	timestamp = (gear_long) time(NULL);

	GEAR_PARSE_PARAMETERS_START(1, 2)
		Z_PARAM_STR(format)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(timestamp)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (ZSTR_LEN(format) == 0) {
		RETURN_FALSE;
	}

	ts = timelib_time_ctor();
	if (gmt) {
		tzi = NULL;
		timelib_unixtime2gmt(ts, (timelib_sll) timestamp);
	} else {
		tzi = get_timezone_info();
		ts->tz_info = tzi;
		ts->zone_type = TIMELIB_ZONETYPE_ID;
		timelib_unixtime2local(ts, (timelib_sll) timestamp);
	}
	ta.tm_sec   = ts->s;
	ta.tm_min   = ts->i;
	ta.tm_hour  = ts->h;
	ta.tm_mday  = ts->d;
	ta.tm_mon   = ts->m - 1;
	ta.tm_year  = ts->y - 1900;
	ta.tm_wday  = timelib_day_of_week(ts->y, ts->m, ts->d);
	ta.tm_yday  = timelib_day_of_year(ts->y, ts->m, ts->d);
	if (gmt) {
		ta.tm_isdst = 0;
#if HAVE_TM_GMTOFF
		ta.tm_gmtoff = 0;
#endif
#if HAVE_TM_ZONE
		ta.tm_zone = "GMT";
#endif
	} else {
		offset = timelib_get_time_zone_info(timestamp, tzi);

		ta.tm_isdst = offset->is_dst;
#if HAVE_TM_GMTOFF
		ta.tm_gmtoff = offset->offset;
#endif
#if HAVE_TM_ZONE
		ta.tm_zone = offset->abbr;
#endif
	}

	/* VS2012 crt has a bug where strftime crash with %z and %Z format when the
	   initial buffer is too small. See
	   http://connect.microsoft.com/VisualStudio/feedback/details/759720/vs2012-strftime-crash-with-z-formatting-code */
	buf = gear_string_alloc(buf_len, 0);
	while ((real_len = strftime(ZSTR_VAL(buf), buf_len, ZSTR_VAL(format), &ta)) == buf_len || real_len == 0) {
		buf_len *= 2;
		buf = gear_string_extend(buf, buf_len, 0);
		if (!--max_reallocs) {
			break;
		}
	}
#ifdef HYSS_WIN32
	/* VS2012 strftime() returns number of characters, not bytes.
		See VC++11 bug id 766205. */
	if (real_len > 0) {
		real_len = strlen(buf->val);
	}
#endif

	timelib_time_dtor(ts);
	if (!gmt) {
		timelib_time_offset_dtor(offset);
	}

	if (real_len && real_len != buf_len) {
		buf = gear_string_truncate(buf, real_len, 0);
		RETURN_NEW_STR(buf);
	}
	gear_string_efree(buf);
	RETURN_FALSE;
}
/* }}} */

/* {{{ proto string strftime(string format [, int timestamp])
   Format a local time/date according to locale settings */
HYSS_FUNCTION(strftime)
{
	hyss_strftime(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto string gmstrftime(string format [, int timestamp])
   Format a GMT/UCT time/date according to locale settings */
HYSS_FUNCTION(gmstrftime)
{
	hyss_strftime(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */
#endif

/* {{{ proto int time(void)
   Return current UNIX timestamp */
HYSS_FUNCTION(time)
{
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	RETURN_LONG((gear_long)time(NULL));
}
/* }}} */

/* {{{ proto array localtime([int timestamp [, bool associative_array]])
   Returns the results of the C system call localtime as an associative array if the associative_array argument is set to 1 other wise it is a regular array */
HYSS_FUNCTION(localtime)
{
	gear_long timestamp = (gear_long)time(NULL);
	gear_bool associative = 0;
	timelib_tzinfo *tzi;
	timelib_time   *ts;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(timestamp)
		Z_PARAM_BOOL(associative)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	tzi = get_timezone_info();
	ts = timelib_time_ctor();
	ts->tz_info = tzi;
	ts->zone_type = TIMELIB_ZONETYPE_ID;
	timelib_unixtime2local(ts, (timelib_sll) timestamp);

	array_init(return_value);

	if (associative) {
		add_assoc_long(return_value, "tm_sec",   ts->s);
		add_assoc_long(return_value, "tm_min",   ts->i);
		add_assoc_long(return_value, "tm_hour",  ts->h);
		add_assoc_long(return_value, "tm_mday",  ts->d);
		add_assoc_long(return_value, "tm_mon",   ts->m - 1);
		add_assoc_long(return_value, "tm_year",  ts->y - 1900);
		add_assoc_long(return_value, "tm_wday",  timelib_day_of_week(ts->y, ts->m, ts->d));
		add_assoc_long(return_value, "tm_yday",  timelib_day_of_year(ts->y, ts->m, ts->d));
		add_assoc_long(return_value, "tm_isdst", ts->dst);
	} else {
		add_next_index_long(return_value, ts->s);
		add_next_index_long(return_value, ts->i);
		add_next_index_long(return_value, ts->h);
		add_next_index_long(return_value, ts->d);
		add_next_index_long(return_value, ts->m - 1);
		add_next_index_long(return_value, ts->y- 1900);
		add_next_index_long(return_value, timelib_day_of_week(ts->y, ts->m, ts->d));
		add_next_index_long(return_value, timelib_day_of_year(ts->y, ts->m, ts->d));
		add_next_index_long(return_value, ts->dst);
	}

	timelib_time_dtor(ts);
}
/* }}} */

/* {{{ proto array getdate([int timestamp])
   Get date/time information */
HYSS_FUNCTION(getdate)
{
	gear_long timestamp = (gear_long)time(NULL);
	timelib_tzinfo *tzi;
	timelib_time   *ts;

	GEAR_PARSE_PARAMETERS_START(0, 1)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(timestamp)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	tzi = get_timezone_info();
	ts = timelib_time_ctor();
	ts->tz_info = tzi;
	ts->zone_type = TIMELIB_ZONETYPE_ID;
	timelib_unixtime2local(ts, (timelib_sll) timestamp);

	array_init(return_value);

	add_assoc_long(return_value, "seconds", ts->s);
	add_assoc_long(return_value, "minutes", ts->i);
	add_assoc_long(return_value, "hours", ts->h);
	add_assoc_long(return_value, "mday", ts->d);
	add_assoc_long(return_value, "wday", timelib_day_of_week(ts->y, ts->m, ts->d));
	add_assoc_long(return_value, "mon", ts->m);
	add_assoc_long(return_value, "year", ts->y);
	add_assoc_long(return_value, "yday", timelib_day_of_year(ts->y, ts->m, ts->d));
	add_assoc_string(return_value, "weekday", hyss_date_full_day_name(ts->y, ts->m, ts->d));
	add_assoc_string(return_value, "month", mon_full_names[ts->m - 1]);
	add_index_long(return_value, 0, timestamp);

	timelib_time_dtor(ts);
}
/* }}} */

#define HYSS_DATE_TIMEZONE_GROUP_AFRICA     0x0001
#define HYSS_DATE_TIMEZONE_GROUP_AMERICA    0x0002
#define HYSS_DATE_TIMEZONE_GROUP_ANTARCTICA 0x0004
#define HYSS_DATE_TIMEZONE_GROUP_ARCTIC     0x0008
#define HYSS_DATE_TIMEZONE_GROUP_ASIA       0x0010
#define HYSS_DATE_TIMEZONE_GROUP_ATLANTIC   0x0020
#define HYSS_DATE_TIMEZONE_GROUP_AUSTRALIA  0x0040
#define HYSS_DATE_TIMEZONE_GROUP_EUROPE     0x0080
#define HYSS_DATE_TIMEZONE_GROUP_INDIAN     0x0100
#define HYSS_DATE_TIMEZONE_GROUP_PACIFIC    0x0200
#define HYSS_DATE_TIMEZONE_GROUP_UTC        0x0400
#define HYSS_DATE_TIMEZONE_GROUP_ALL        0x07FF
#define HYSS_DATE_TIMEZONE_GROUP_ALL_W_BC   0x0FFF
#define HYSS_DATE_TIMEZONE_PER_COUNTRY      0x1000

#define HYSS_DATE_PERIOD_EXCLUDE_START_DATE 0x0001


/* define an overloaded iterator structure */
typedef struct {
	gear_object_iterator  intern;
	zval                  current;
	hyss_period_obj       *object;
	int                   current_index;
} date_period_it;

/* {{{ date_period_it_invalidate_current */
static void date_period_it_invalidate_current(gear_object_iterator *iter)
{
	date_period_it *iterator = (date_period_it *)iter;

	if (Z_TYPE(iterator->current) != IS_UNDEF) {
		zval_ptr_dtor(&iterator->current);
		ZVAL_UNDEF(&iterator->current);
	}
}
/* }}} */

/* {{{ date_period_it_dtor */
static void date_period_it_dtor(gear_object_iterator *iter)
{
	date_period_it *iterator = (date_period_it *)iter;

	date_period_it_invalidate_current(iter);

	zval_ptr_dtor(&iterator->intern.data);
}
/* }}} */

/* {{{ date_period_it_has_more */
static int date_period_it_has_more(gear_object_iterator *iter)
{
	date_period_it *iterator = (date_period_it *)iter;
	hyss_period_obj *object   = Z_HYSSPERIOD_P(&iterator->intern.data);
	timelib_time   *it_time = object->current;

	/* apply modification if it's not the first iteration */
	if (!object->include_start_date || iterator->current_index > 0) {
		it_time->have_relative = 1;
		it_time->relative = *object->interval;
		it_time->sse_uptodate = 0;
		timelib_update_ts(it_time, NULL);
		timelib_update_from_sse(it_time);
	}

	if (object->end) {
		return object->current->sse < object->end->sse ? SUCCESS : FAILURE;
	} else {
		return (iterator->current_index < object->recurrences) ? SUCCESS : FAILURE;
	}
}
/* }}} */

/* {{{ date_period_it_current_data */
static zval *date_period_it_current_data(gear_object_iterator *iter)
{
	date_period_it *iterator = (date_period_it *)iter;
	hyss_period_obj *object   = Z_HYSSPERIOD_P(&iterator->intern.data);
	timelib_time   *it_time = object->current;
	hyss_date_obj   *newdateobj;

	/* Create new object */
	hyss_date_instantiate(object->start_ce, &iterator->current);
	newdateobj = Z_HYSSDATE_P(&iterator->current);
	newdateobj->time = timelib_time_ctor();
	*newdateobj->time = *it_time;
	if (it_time->tz_abbr) {
		newdateobj->time->tz_abbr = timelib_strdup(it_time->tz_abbr);
	}
	if (it_time->tz_info) {
		newdateobj->time->tz_info = it_time->tz_info;
	}

	return &iterator->current;
}
/* }}} */

/* {{{ date_period_it_current_key */
static void date_period_it_current_key(gear_object_iterator *iter, zval *key)
{
	date_period_it *iterator = (date_period_it *)iter;
	ZVAL_LONG(key, iterator->current_index);
}
/* }}} */

/* {{{ date_period_it_move_forward */
static void date_period_it_move_forward(gear_object_iterator *iter)
{
	date_period_it   *iterator = (date_period_it *)iter;

	iterator->current_index++;
	date_period_it_invalidate_current(iter);
}
/* }}} */

/* {{{ date_period_it_rewind */
static void date_period_it_rewind(gear_object_iterator *iter)
{
	date_period_it *iterator = (date_period_it *)iter;

	iterator->current_index = 0;
	if (iterator->object->current) {
		timelib_time_dtor(iterator->object->current);
	}
	if (!iterator->object->start) {
		gear_throw_error(NULL, "DatePeriod has not been initialized correctly");
		return;
	}
	iterator->object->current = timelib_time_clone(iterator->object->start);
	date_period_it_invalidate_current(iter);
}
/* }}} */

/* iterator handler table */
static const gear_object_iterator_funcs date_period_it_funcs = {
	date_period_it_dtor,
	date_period_it_has_more,
	date_period_it_current_data,
	date_period_it_current_key,
	date_period_it_move_forward,
	date_period_it_rewind,
	date_period_it_invalidate_current
};

gear_object_iterator *date_object_period_get_iterator(gear_class_entry *ce, zval *object, int by_ref) /* {{{ */
{
	date_period_it *iterator;

	if (by_ref) {
		gear_throw_error(NULL, "An iterator cannot be used with foreach by reference");
		return NULL;
	}

	iterator = emalloc(sizeof(date_period_it));

	gear_iterator_init((gear_object_iterator*)iterator);

	ZVAL_COPY(&iterator->intern.data, object);
	iterator->intern.funcs = &date_period_it_funcs;
	iterator->object = Z_HYSSPERIOD_P(object);
	ZVAL_UNDEF(&iterator->current);

	return (gear_object_iterator*)iterator;
} /* }}} */

static int implement_date_interface_handler(gear_class_entry *interface, gear_class_entry *implementor) /* {{{ */
{
	if (implementor->type == GEAR_USER_CLASS &&
		!instanceof_function(implementor, date_ce_date) &&
		!instanceof_function(implementor, date_ce_immutable)
	) {
		gear_error(E_ERROR, "DateTimeInterface can't be implemented by user classes");
	}

	return SUCCESS;
} /* }}} */

static int date_interval_has_property(zval *object, zval *member, int type, void **cache_slot) /* {{{ */
{
	hyss_interval_obj *obj;
	zval tmp_member;
	zval rv;
	zval *prop;
	int retval = 0;

	if (UNEXPECTED(Z_TYPE_P(member) != IS_STRING)) {
		ZVAL_STR(&tmp_member, zval_get_string_func(member));
		member = &tmp_member;
		cache_slot = NULL;
	}

	obj = Z_HYSSINTERVAL_P(object);

	if (!obj->initialized) {
		retval = gear_std_has_property(object, member, type, cache_slot);
		if (member == &tmp_member) {
			zval_ptr_dtor_str(&tmp_member);
		}
		return retval;
	}

	prop = date_interval_read_property(object, member, BP_VAR_IS, cache_slot, &rv);

	if (prop != &EG(uninitialized_zval)) {
		if (type == 2) {
			retval = 1;
		} else if (type == 1) {
			retval = gear_is_true(prop);
		} else if (type == 0) {
			retval = (Z_TYPE_P(prop) != IS_NULL);
		}
	} else {
		retval = gear_std_has_property(object, member, type, cache_slot);
	}

	if (member == &tmp_member) {
		zval_ptr_dtor_str(&tmp_member);
	}

	return retval;

}
/* }}} */

static void date_register_classes(void) /* {{{ */
{
	gear_class_entry ce_date, ce_immutable, ce_timezone, ce_interval, ce_period, ce_interface;

	INIT_CLASS_ENTRY(ce_interface, "DateTimeInterface", date_funcs_interface);
	date_ce_interface = gear_register_internal_interface(&ce_interface);
	date_ce_interface->interface_gets_implemented = implement_date_interface_handler;

#define REGISTER_DATE_INTERFACE_CONST_STRING(const_name, value) \
	gear_declare_class_constant_stringl(date_ce_interface, const_name, sizeof(const_name)-1, value, sizeof(value)-1);

	REGISTER_DATE_INTERFACE_CONST_STRING("ATOM",             DATE_FORMAT_RFC3339);
	REGISTER_DATE_INTERFACE_CONST_STRING("COOKIE",           DATE_FORMAT_COOKIE);
	REGISTER_DATE_INTERFACE_CONST_STRING("ISO8601",          DATE_FORMAT_ISO8601);
	REGISTER_DATE_INTERFACE_CONST_STRING("RFC822",           DATE_FORMAT_RFC822);
	REGISTER_DATE_INTERFACE_CONST_STRING("RFC850",           DATE_FORMAT_RFC850);
	REGISTER_DATE_INTERFACE_CONST_STRING("RFC1036",          DATE_FORMAT_RFC1036);
	REGISTER_DATE_INTERFACE_CONST_STRING("RFC1123",          DATE_FORMAT_RFC1123);
	REGISTER_DATE_INTERFACE_CONST_STRING("RFC7231",          DATE_FORMAT_RFC7231);
	REGISTER_DATE_INTERFACE_CONST_STRING("RFC2822",          DATE_FORMAT_RFC2822);
	REGISTER_DATE_INTERFACE_CONST_STRING("RFC3339",          DATE_FORMAT_RFC3339);
	REGISTER_DATE_INTERFACE_CONST_STRING("RFC3339_EXTENDED", DATE_FORMAT_RFC3339_EXTENDED);
	REGISTER_DATE_INTERFACE_CONST_STRING("RSS",              DATE_FORMAT_RFC1123);
	REGISTER_DATE_INTERFACE_CONST_STRING("W3C",              DATE_FORMAT_RFC3339);

	INIT_CLASS_ENTRY(ce_date, "DateTime", date_funcs_date);
	ce_date.create_object = date_object_new_date;
	date_ce_date = gear_register_internal_class_ex(&ce_date, NULL);
	memcpy(&date_object_handlers_date, &std_object_handlers, sizeof(gear_object_handlers));
	date_object_handlers_date.offset = XtOffsetOf(hyss_date_obj, std);
	date_object_handlers_date.free_obj = date_object_free_storage_date;
	date_object_handlers_date.clone_obj = date_object_clone_date;
	date_object_handlers_date.compare_objects = date_object_compare_date;
	date_object_handlers_date.get_properties = date_object_get_properties;
	date_object_handlers_date.get_gc = date_object_get_gc;
	gear_class_implements(date_ce_date, 1, date_ce_interface);

	INIT_CLASS_ENTRY(ce_immutable, "DateTimeImmutable", date_funcs_immutable);
	ce_immutable.create_object = date_object_new_date;
	date_ce_immutable = gear_register_internal_class_ex(&ce_immutable, NULL);
	memcpy(&date_object_handlers_immutable, &std_object_handlers, sizeof(gear_object_handlers));
	date_object_handlers_immutable.clone_obj = date_object_clone_date;
	date_object_handlers_immutable.compare_objects = date_object_compare_date;
	date_object_handlers_immutable.get_properties = date_object_get_properties;
	date_object_handlers_immutable.get_gc = date_object_get_gc;
	gear_class_implements(date_ce_immutable, 1, date_ce_interface);

	INIT_CLASS_ENTRY(ce_timezone, "DateTimeZone", date_funcs_timezone);
	ce_timezone.create_object = date_object_new_timezone;
	date_ce_timezone = gear_register_internal_class_ex(&ce_timezone, NULL);
	memcpy(&date_object_handlers_timezone, &std_object_handlers, sizeof(gear_object_handlers));
	date_object_handlers_timezone.offset = XtOffsetOf(hyss_timezone_obj, std);
	date_object_handlers_timezone.free_obj = date_object_free_storage_timezone;
	date_object_handlers_timezone.clone_obj = date_object_clone_timezone;
	date_object_handlers_timezone.get_properties = date_object_get_properties_timezone;
	date_object_handlers_timezone.get_gc = date_object_get_gc_timezone;
	date_object_handlers_timezone.get_debug_info = date_object_get_debug_info_timezone;

#define REGISTER_TIMEZONE_CLASS_CONST_STRING(const_name, value) \
	gear_declare_class_constant_long(date_ce_timezone, const_name, sizeof(const_name)-1, value);

	REGISTER_TIMEZONE_CLASS_CONST_STRING("AFRICA",      HYSS_DATE_TIMEZONE_GROUP_AFRICA);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("AMERICA",     HYSS_DATE_TIMEZONE_GROUP_AMERICA);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("ANTARCTICA",  HYSS_DATE_TIMEZONE_GROUP_ANTARCTICA);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("ARCTIC",      HYSS_DATE_TIMEZONE_GROUP_ARCTIC);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("ASIA",        HYSS_DATE_TIMEZONE_GROUP_ASIA);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("ATLANTIC",    HYSS_DATE_TIMEZONE_GROUP_ATLANTIC);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("AUSTRALIA",   HYSS_DATE_TIMEZONE_GROUP_AUSTRALIA);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("EUROPE",      HYSS_DATE_TIMEZONE_GROUP_EUROPE);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("INDIAN",      HYSS_DATE_TIMEZONE_GROUP_INDIAN);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("PACIFIC",     HYSS_DATE_TIMEZONE_GROUP_PACIFIC);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("UTC",         HYSS_DATE_TIMEZONE_GROUP_UTC);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("ALL",         HYSS_DATE_TIMEZONE_GROUP_ALL);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("ALL_WITH_BC", HYSS_DATE_TIMEZONE_GROUP_ALL_W_BC);
	REGISTER_TIMEZONE_CLASS_CONST_STRING("PER_COUNTRY", HYSS_DATE_TIMEZONE_PER_COUNTRY);

	INIT_CLASS_ENTRY(ce_interval, "DateInterval", date_funcs_interval);
	ce_interval.create_object = date_object_new_interval;
	date_ce_interval = gear_register_internal_class_ex(&ce_interval, NULL);
	memcpy(&date_object_handlers_interval, &std_object_handlers, sizeof(gear_object_handlers));
	date_object_handlers_interval.offset = XtOffsetOf(hyss_interval_obj, std);
	date_object_handlers_interval.free_obj = date_object_free_storage_interval;
	date_object_handlers_interval.clone_obj = date_object_clone_interval;
	date_object_handlers_interval.has_property = date_interval_has_property;
	date_object_handlers_interval.read_property = date_interval_read_property;
	date_object_handlers_interval.write_property = date_interval_write_property;
	date_object_handlers_interval.get_properties = date_object_get_properties_interval;
	date_object_handlers_interval.get_property_ptr_ptr = date_interval_get_property_ptr_ptr;
	date_object_handlers_interval.get_gc = date_object_get_gc_interval;

	INIT_CLASS_ENTRY(ce_period, "DatePeriod", date_funcs_period);
	ce_period.create_object = date_object_new_period;
	date_ce_period = gear_register_internal_class_ex(&ce_period, NULL);
	date_ce_period->get_iterator = date_object_period_get_iterator;
	gear_class_implements(date_ce_period, 1, gear_ce_traversable);
	memcpy(&date_object_handlers_period, &std_object_handlers, sizeof(gear_object_handlers));
	date_object_handlers_period.offset = XtOffsetOf(hyss_period_obj, std);
	date_object_handlers_period.free_obj = date_object_free_storage_period;
	date_object_handlers_period.clone_obj = date_object_clone_period;
	date_object_handlers_period.get_properties = date_object_get_properties_period;
	date_object_handlers_period.get_property_ptr_ptr = NULL;
	date_object_handlers_period.get_gc = date_object_get_gc_period;
	date_object_handlers_period.read_property = date_period_read_property;
	date_object_handlers_period.write_property = date_period_write_property;

#define REGISTER_PERIOD_CLASS_CONST_STRING(const_name, value) \
	gear_declare_class_constant_long(date_ce_period, const_name, sizeof(const_name)-1, value);

	REGISTER_PERIOD_CLASS_CONST_STRING("EXCLUDE_START_DATE", HYSS_DATE_PERIOD_EXCLUDE_START_DATE);
} /* }}} */

static gear_object *date_object_new_date(gear_class_entry *class_type) /* {{{ */
{
	hyss_date_obj *intern = gear_object_alloc(sizeof(hyss_date_obj), class_type);

	gear_object_std_init(&intern->std, class_type);
	object_properties_init(&intern->std, class_type);
	intern->std.handlers = &date_object_handlers_date;

	return &intern->std;
} /* }}} */

static gear_object *date_object_clone_date(zval *this_ptr) /* {{{ */
{
	hyss_date_obj *old_obj = Z_HYSSDATE_P(this_ptr);
	hyss_date_obj *new_obj = hyss_date_obj_from_obj(date_object_new_date(old_obj->std.ce));

	gear_objects_clone_members(&new_obj->std, &old_obj->std);
	if (!old_obj->time) {
		return &new_obj->std;
	}

	/* this should probably moved to a new `timelib_time *timelime_time_clone(timelib_time *)` */
	new_obj->time = timelib_time_ctor();
	*new_obj->time = *old_obj->time;
	if (old_obj->time->tz_abbr) {
		new_obj->time->tz_abbr = timelib_strdup(old_obj->time->tz_abbr);
	}
	if (old_obj->time->tz_info) {
		new_obj->time->tz_info = old_obj->time->tz_info;
	}

	return &new_obj->std;
} /* }}} */

static void date_clone_immutable(zval *object, zval *new_object) /* {{{ */
{
	ZVAL_OBJ(new_object, date_object_clone_date(object));
} /* }}} */

static int date_object_compare_date(zval *d1, zval *d2) /* {{{ */
{
	hyss_date_obj *o1 = Z_HYSSDATE_P(d1);
	hyss_date_obj *o2 = Z_HYSSDATE_P(d2);

	if (!o1->time || !o2->time) {
		hyss_error_docref(NULL, E_WARNING, "Trying to compare an incomplete DateTime or DateTimeImmutable object");
		return 1;
	}
	if (!o1->time->sse_uptodate) {
		timelib_update_ts(o1->time, o1->time->tz_info);
	}
	if (!o2->time->sse_uptodate) {
		timelib_update_ts(o2->time, o2->time->tz_info);
	}

	return timelib_time_compare(o1->time, o2->time);
} /* }}} */

static HashTable *date_object_get_gc(zval *object, zval **table, int *n) /* {{{ */
{
	*table = NULL;
	*n = 0;
	return gear_std_get_properties(object);
} /* }}} */

static HashTable *date_object_get_gc_timezone(zval *object, zval **table, int *n) /* {{{ */
{
       *table = NULL;
       *n = 0;
       return gear_std_get_properties(object);
} /* }}} */

static HashTable *date_object_get_properties(zval *object) /* {{{ */
{
	HashTable *props;
	zval zv;
	hyss_date_obj     *dateobj;


	dateobj = Z_HYSSDATE_P(object);

	props = gear_std_get_properties(object);

	if (!dateobj->time) {
		return props;
	}

	/* first we add the date and time in ISO format */
	ZVAL_STR(&zv, date_format("Y-m-d H:i:s.u", sizeof("Y-m-d H:i:s.u")-1, dateobj->time, 1));
	gear_hash_str_update(props, "date", sizeof("date")-1, &zv);

	/* then we add the timezone name (or similar) */
	if (dateobj->time->is_localtime) {
		ZVAL_LONG(&zv, dateobj->time->zone_type);
		gear_hash_str_update(props, "timezone_type", sizeof("timezone_type")-1, &zv);

		switch (dateobj->time->zone_type) {
			case TIMELIB_ZONETYPE_ID:
				ZVAL_STRING(&zv, dateobj->time->tz_info->name);
				break;
			case TIMELIB_ZONETYPE_OFFSET: {
				gear_string *tmpstr = gear_string_alloc(sizeof("UTC+05:00")-1, 0);
				int utc_offset = dateobj->time->z;

				ZSTR_LEN(tmpstr) = snprintf(ZSTR_VAL(tmpstr), sizeof("+05:00"), "%c%02d:%02d",
					utc_offset < 0 ? '-' : '+',
					abs(utc_offset / 3600),
					abs(((utc_offset % 3600) / 60)));

				ZVAL_NEW_STR(&zv, tmpstr);
				}
				break;
			case TIMELIB_ZONETYPE_ABBR:
				ZVAL_STRING(&zv, dateobj->time->tz_abbr);
				break;
		}
		gear_hash_str_update(props, "timezone", sizeof("timezone")-1, &zv);
	}

	return props;
} /* }}} */

static gear_object *date_object_new_timezone(gear_class_entry *class_type) /* {{{ */
{
	hyss_timezone_obj *intern = gear_object_alloc(sizeof(hyss_timezone_obj), class_type);

	gear_object_std_init(&intern->std, class_type);
	object_properties_init(&intern->std, class_type);
	intern->std.handlers = &date_object_handlers_timezone;

	return &intern->std;
} /* }}} */

static gear_object *date_object_clone_timezone(zval *this_ptr) /* {{{ */
{
	hyss_timezone_obj *old_obj = Z_HYSSTIMEZONE_P(this_ptr);
	hyss_timezone_obj *new_obj = hyss_timezone_obj_from_obj(date_object_new_timezone(old_obj->std.ce));

	gear_objects_clone_members(&new_obj->std, &old_obj->std);
	if (!old_obj->initialized) {
		return &new_obj->std;
	}

	new_obj->type = old_obj->type;
	new_obj->initialized = 1;
	switch (new_obj->type) {
		case TIMELIB_ZONETYPE_ID:
			new_obj->tzi.tz = old_obj->tzi.tz;
			break;
		case TIMELIB_ZONETYPE_OFFSET:
			new_obj->tzi.utc_offset = old_obj->tzi.utc_offset;
			break;
		case TIMELIB_ZONETYPE_ABBR:
			new_obj->tzi.z.utc_offset = old_obj->tzi.z.utc_offset;
			new_obj->tzi.z.dst        = old_obj->tzi.z.dst;
			new_obj->tzi.z.abbr       = timelib_strdup(old_obj->tzi.z.abbr);
			break;
	}

	return &new_obj->std;
} /* }}} */

static void hyss_timezone_to_string(hyss_timezone_obj *tzobj, zval *zv)
{
	switch (tzobj->type) {
		case TIMELIB_ZONETYPE_ID:
			ZVAL_STRING(zv, tzobj->tzi.tz->name);
			break;
		case TIMELIB_ZONETYPE_OFFSET: {
			gear_string *tmpstr = gear_string_alloc(sizeof("UTC+05:00")-1, 0);
			timelib_sll utc_offset = tzobj->tzi.utc_offset;

			ZSTR_LEN(tmpstr) = snprintf(ZSTR_VAL(tmpstr), sizeof("+05:00"), "%c%02d:%02d",
				utc_offset < 0 ? '-' : '+',
				abs((int)(utc_offset / 3600)),
				abs((int)(utc_offset % 3600) / 60));

			ZVAL_NEW_STR(zv, tmpstr);
			}
			break;
		case TIMELIB_ZONETYPE_ABBR:
			ZVAL_STRING(zv, tzobj->tzi.z.abbr);
			break;
	}
}

static HashTable *date_object_get_properties_timezone(zval *object) /* {{{ */
{
	HashTable *props;
	zval zv;
	hyss_timezone_obj     *tzobj;

	tzobj = Z_HYSSTIMEZONE_P(object);

	props = gear_std_get_properties(object);

	if (!tzobj->initialized) {
		return props;
	}

	ZVAL_LONG(&zv, tzobj->type);
	gear_hash_str_update(props, "timezone_type", sizeof("timezone_type")-1, &zv);

	hyss_timezone_to_string(tzobj, &zv);
	gear_hash_str_update(props, "timezone", sizeof("timezone")-1, &zv);

	return props;
} /* }}} */

static HashTable *date_object_get_debug_info_timezone(zval *object, int *is_temp) /* {{{ */
{
	HashTable *ht, *props;
	zval zv;
	hyss_timezone_obj *tzobj;

	tzobj = Z_HYSSTIMEZONE_P(object);
	props = gear_std_get_properties(object);

	*is_temp = 1;
	ht = gear_array_dup(props);

	ZVAL_LONG(&zv, tzobj->type);
	gear_hash_str_update(ht, "timezone_type", sizeof("timezone_type")-1, &zv);

	hyss_timezone_to_string(tzobj, &zv);
	gear_hash_str_update(ht, "timezone", sizeof("timezone")-1, &zv);

	return ht;
} /* }}} */

static gear_object *date_object_new_interval(gear_class_entry *class_type) /* {{{ */
{
	hyss_interval_obj *intern = gear_object_alloc(sizeof(hyss_interval_obj), class_type);

	gear_object_std_init(&intern->std, class_type);
	object_properties_init(&intern->std, class_type);
	intern->std.handlers = &date_object_handlers_interval;

	return &intern->std;
} /* }}} */

static gear_object *date_object_clone_interval(zval *this_ptr) /* {{{ */
{
	hyss_interval_obj *old_obj = Z_HYSSINTERVAL_P(this_ptr);
	hyss_interval_obj *new_obj = hyss_interval_obj_from_obj(date_object_new_interval(old_obj->std.ce));

	gear_objects_clone_members(&new_obj->std, &old_obj->std);
	new_obj->initialized = old_obj->initialized;
	if (old_obj->diff) {
		new_obj->diff = timelib_rel_time_clone(old_obj->diff);
	}

	return &new_obj->std;
} /* }}} */

static HashTable *date_object_get_gc_interval(zval *object, zval **table, int *n) /* {{{ */
{

	*table = NULL;
	*n = 0;
	return gear_std_get_properties(object);
} /* }}} */

static HashTable *date_object_get_properties_interval(zval *object) /* {{{ */
{
	HashTable *props;
	zval zv;
	hyss_interval_obj     *intervalobj;

	intervalobj = Z_HYSSINTERVAL_P(object);

	props = gear_std_get_properties(object);

	if (!intervalobj->initialized) {
		return props;
	}

#define HYSS_DATE_INTERVAL_ADD_PROPERTY(n,f) \
	ZVAL_LONG(&zv, (gear_long)intervalobj->diff->f); \
	gear_hash_str_update(props, n, sizeof(n)-1, &zv);

	HYSS_DATE_INTERVAL_ADD_PROPERTY("y", y);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("m", m);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("d", d);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("h", h);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("i", i);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("s", s);
	ZVAL_DOUBLE(&zv, (double)intervalobj->diff->us / 1000000.0);
	gear_hash_str_update(props, "f", sizeof("f") - 1, &zv);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("weekday", weekday);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("weekday_behavior", weekday_behavior);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("first_last_day_of", first_last_day_of);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("invert", invert);
	if (intervalobj->diff->days != -99999) {
		HYSS_DATE_INTERVAL_ADD_PROPERTY("days", days);
	} else {
		ZVAL_FALSE(&zv);
		gear_hash_str_update(props, "days", sizeof("days")-1, &zv);
	}
	HYSS_DATE_INTERVAL_ADD_PROPERTY("special_type", special.type);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("special_amount", special.amount);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("have_weekday_relative", have_weekday_relative);
	HYSS_DATE_INTERVAL_ADD_PROPERTY("have_special_relative", have_special_relative);

	return props;
} /* }}} */

static gear_object *date_object_new_period(gear_class_entry *class_type) /* {{{ */
{
	hyss_period_obj *intern = gear_object_alloc(sizeof(hyss_period_obj), class_type);

	gear_object_std_init(&intern->std, class_type);
	object_properties_init(&intern->std, class_type);

	intern->std.handlers = &date_object_handlers_period;

	return &intern->std;
} /* }}} */

static gear_object *date_object_clone_period(zval *this_ptr) /* {{{ */
{
	hyss_period_obj *old_obj = Z_HYSSPERIOD_P(this_ptr);
	hyss_period_obj *new_obj = hyss_period_obj_from_obj(date_object_new_period(old_obj->std.ce));

	gear_objects_clone_members(&new_obj->std, &old_obj->std);
	new_obj->initialized = old_obj->initialized;
	new_obj->recurrences = old_obj->recurrences;
	new_obj->include_start_date = old_obj->include_start_date;
	new_obj->start_ce = old_obj->start_ce;

	if (old_obj->start) {
		new_obj->start = timelib_time_clone(old_obj->start);
	}
	if (old_obj->current) {
		new_obj->current = timelib_time_clone(old_obj->current);
	}
	if (old_obj->end) {
        new_obj->end = timelib_time_clone(old_obj->end);
    }
    if (old_obj->interval) {
        new_obj->interval = timelib_rel_time_clone(old_obj->interval);
    }
	return &new_obj->std;
} /* }}} */

static void date_object_free_storage_date(gear_object *object) /* {{{ */
{
	hyss_date_obj *intern = hyss_date_obj_from_obj(object);

	if (intern->time) {
		timelib_time_dtor(intern->time);
	}

	gear_object_std_dtor(&intern->std);
} /* }}} */

static void date_object_free_storage_timezone(gear_object *object) /* {{{ */
{
	hyss_timezone_obj *intern = hyss_timezone_obj_from_obj(object);

	if (intern->type == TIMELIB_ZONETYPE_ABBR) {
		timelib_free(intern->tzi.z.abbr);
	}
	gear_object_std_dtor(&intern->std);
} /* }}} */

static void date_object_free_storage_interval(gear_object *object) /* {{{ */
{
	hyss_interval_obj *intern = hyss_interval_obj_from_obj(object);

	timelib_rel_time_dtor(intern->diff);
	gear_object_std_dtor(&intern->std);
} /* }}} */

static void date_object_free_storage_period(gear_object *object) /* {{{ */
{
	hyss_period_obj *intern = hyss_period_obj_from_obj(object);

	if (intern->start) {
		timelib_time_dtor(intern->start);
	}

	if (intern->current) {
		timelib_time_dtor(intern->current);
	}

	if (intern->end) {
		timelib_time_dtor(intern->end);
	}

	timelib_rel_time_dtor(intern->interval);
	gear_object_std_dtor(&intern->std);
} /* }}} */

/* Advanced Interface */
HYSSAPI zval *hyss_date_instantiate(gear_class_entry *pce, zval *object) /* {{{ */
{
	object_init_ex(object, pce);
	return object;
} /* }}} */

/* Helper function used to store the latest found warnings and errors while
 * parsing, from either strtotime or parse_from_format. */
static void update_errors_warnings(timelib_error_container *last_errors) /* {{{ */
{
	if (DATEG(last_errors)) {
		timelib_error_container_dtor(DATEG(last_errors));
		DATEG(last_errors) = NULL;
	}
	DATEG(last_errors) = last_errors;
} /* }}} */

static void hyss_date_set_time_fraction(timelib_time *time, int microseconds)
{
	time->us = microseconds;
}

static void hyss_date_get_current_time_with_fraction(time_t *sec, suseconds_t *usec)
{
#if HAVE_GETTIMEOFDAY
	struct timeval tp = {0}; /* For setting microseconds */

	gettimeofday(&tp, NULL);
	*sec = tp.tv_sec;
	*usec = tp.tv_usec;
#else
	*sec = time(NULL);
	*usec = 0;
#endif
}

HYSSAPI int hyss_date_initialize(hyss_date_obj *dateobj, /*const*/ char *time_str, size_t time_str_len, char *format, zval *timezone_object, int ctor) /* {{{ */
{
	timelib_time   *now;
	timelib_tzinfo *tzi = NULL;
	timelib_error_container *err = NULL;
	int type = TIMELIB_ZONETYPE_ID, new_dst = 0;
	char *new_abbr = NULL;
	timelib_sll new_offset = 0;
	time_t sec;
	suseconds_t usec;

	if (dateobj->time) {
		timelib_time_dtor(dateobj->time);
	}
	if (format) {
		dateobj->time = timelib_parse_from_format(format, time_str_len ? time_str : "", time_str_len ? time_str_len : 0, &err, DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);
	} else {
		dateobj->time = timelib_strtotime(time_str_len ? time_str : "now", time_str_len ? time_str_len : sizeof("now") -1, &err, DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);
	}

	/* update last errors and warnings */
	update_errors_warnings(err);


	if (ctor && err && err->error_count) {
		/* spit out the first library error message, at least */
		hyss_error_docref(NULL, E_WARNING, "Failed to parse time string (%s) at position %d (%c): %s", time_str,
			err->error_messages[0].position, err->error_messages[0].character, err->error_messages[0].message);
	}
	if (err && err->error_count) {
		timelib_time_dtor(dateobj->time);
		dateobj->time = 0;
		return 0;
	}

	if (timezone_object) {
		hyss_timezone_obj *tzobj;

		tzobj = Z_HYSSTIMEZONE_P(timezone_object);
		switch (tzobj->type) {
			case TIMELIB_ZONETYPE_ID:
				tzi = tzobj->tzi.tz;
				break;
			case TIMELIB_ZONETYPE_OFFSET:
				new_offset = tzobj->tzi.utc_offset;
				break;
			case TIMELIB_ZONETYPE_ABBR:
				new_offset = tzobj->tzi.z.utc_offset;
				new_dst    = tzobj->tzi.z.dst;
				new_abbr   = timelib_strdup(tzobj->tzi.z.abbr);
				break;
		}
		type = tzobj->type;
	} else if (dateobj->time->tz_info) {
		tzi = dateobj->time->tz_info;
	} else {
		tzi = get_timezone_info();
	}

	now = timelib_time_ctor();
	now->zone_type = type;
	switch (type) {
		case TIMELIB_ZONETYPE_ID:
			now->tz_info = tzi;
			break;
		case TIMELIB_ZONETYPE_OFFSET:
			now->z = new_offset;
			break;
		case TIMELIB_ZONETYPE_ABBR:
			now->z = new_offset;
			now->dst = new_dst;
			now->tz_abbr = new_abbr;
			break;
	}
	hyss_date_get_current_time_with_fraction(&sec, &usec);
	timelib_unixtime2local(now, (timelib_sll) sec);
	hyss_date_set_time_fraction(now, usec);
	timelib_fill_holes(dateobj->time, now, TIMELIB_NO_CLONE);
	timelib_update_ts(dateobj->time, tzi);
	timelib_update_from_sse(dateobj->time);

	dateobj->time->have_relative = 0;

	timelib_time_dtor(now);

	return 1;
} /* }}} */

/* {{{ proto DateTime date_create([string time[, DateTimeZone object]])
   Returns new DateTime object
*/
HYSS_FUNCTION(date_create)
{
	zval           *timezone_object = NULL;
	char           *time_str = NULL;
	size_t          time_str_len = 0;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(time_str, time_str_len)
		Z_PARAM_OBJECT_OF_CLASS_EX(timezone_object, date_ce_timezone, 1, 0)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	hyss_date_instantiate(date_ce_date, return_value);
	if (!hyss_date_initialize(Z_HYSSDATE_P(return_value), time_str, time_str_len, NULL, timezone_object, 0)) {
		zval_ptr_dtor(return_value);
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto DateTime date_create_immutable([string time[, DateTimeZone object]])
   Returns new DateTime object
*/
HYSS_FUNCTION(date_create_immutable)
{
	zval           *timezone_object = NULL;
	char           *time_str = NULL;
	size_t          time_str_len = 0;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(time_str, time_str_len)
		Z_PARAM_OBJECT_OF_CLASS_EX(timezone_object, date_ce_timezone, 1, 0)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	hyss_date_instantiate(date_ce_immutable, return_value);
	if (!hyss_date_initialize(Z_HYSSDATE_P(return_value), time_str, time_str_len, NULL, timezone_object, 0)) {
		zval_ptr_dtor(return_value);
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto DateTime date_create_from_format(string format, string time[, DateTimeZone object])
   Returns new DateTime object formatted according to the specified format
*/
HYSS_FUNCTION(date_create_from_format)
{
	zval           *timezone_object = NULL;
	char           *time_str = NULL, *format_str = NULL;
	size_t          time_str_len = 0, format_str_len = 0;

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STRING(format_str, format_str_len)
		Z_PARAM_STRING(time_str, time_str_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_OBJECT_OF_CLASS_EX(timezone_object, date_ce_timezone, 1, 0)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	hyss_date_instantiate(date_ce_date, return_value);
	if (!hyss_date_initialize(Z_HYSSDATE_P(return_value), time_str, time_str_len, format_str, timezone_object, 0)) {
		zval_ptr_dtor(return_value);
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto DateTime date_create_immutable_from_format(string format, string time[, DateTimeZone object])
   Returns new DateTime object formatted according to the specified format
*/
HYSS_FUNCTION(date_create_immutable_from_format)
{
	zval           *timezone_object = NULL;
	char           *time_str = NULL, *format_str = NULL;
	size_t          time_str_len = 0, format_str_len = 0;

	GEAR_PARSE_PARAMETERS_START(2, 3)
		Z_PARAM_STRING(format_str, format_str_len)
		Z_PARAM_STRING(time_str, time_str_len)
		Z_PARAM_OPTIONAL
		Z_PARAM_OBJECT_OF_CLASS_EX(timezone_object, date_ce_timezone, 1, 0)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	hyss_date_instantiate(date_ce_immutable, return_value);
	if (!hyss_date_initialize(Z_HYSSDATE_P(return_value), time_str, time_str_len, format_str, timezone_object, 0)) {
		zval_ptr_dtor(return_value);
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto DateTime::__construct([string time[, DateTimeZone object]])
   Creates new DateTime object
*/
HYSS_METHOD(DateTime, __construct)
{
	zval *timezone_object = NULL;
	char *time_str = NULL;
	size_t time_str_len = 0;
	gear_error_handling error_handling;

	GEAR_PARSE_PARAMETERS_START_EX(GEAR_PARSE_PARAMS_THROW, 0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(time_str, time_str_len)
		Z_PARAM_OBJECT_OF_CLASS_EX(timezone_object, date_ce_timezone, 1, 0)
	GEAR_PARSE_PARAMETERS_END();

	gear_replace_error_handling(EH_THROW, NULL, &error_handling);
	hyss_date_initialize(Z_HYSSDATE_P(getThis()), time_str, time_str_len, NULL, timezone_object, 1);
	gear_restore_error_handling(&error_handling);
}
/* }}} */

/* {{{ proto DateTimeImmutable::__construct([string time[, DateTimeZone object]])
   Creates new DateTimeImmutable object
*/
HYSS_METHOD(DateTimeImmutable, __construct)
{
	zval *timezone_object = NULL;
	char *time_str = NULL;
	size_t time_str_len = 0;
	gear_error_handling error_handling;

	GEAR_PARSE_PARAMETERS_START_EX(GEAR_PARSE_PARAMS_THROW, 0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_STRING(time_str, time_str_len)
		Z_PARAM_OBJECT_OF_CLASS_EX(timezone_object, date_ce_timezone, 1, 0)
	GEAR_PARSE_PARAMETERS_END();

	gear_replace_error_handling(EH_THROW, NULL, &error_handling);
	hyss_date_initialize(Z_HYSSDATE_P(getThis()), time_str, time_str_len, NULL, timezone_object, 1);
	gear_restore_error_handling(&error_handling);
}
/* }}} */

/* {{{ proto DateTime::createFromImmutable(DateTimeImmutable object)
   Creates new DateTime object from an existing immutable DateTimeImmutable object.
*/
HYSS_METHOD(DateTime, createFromImmutable)
{
	zval *datetimeimmutable_object = NULL;
	hyss_date_obj *new_obj = NULL;
	hyss_date_obj *old_obj = NULL;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(datetimeimmutable_object, date_ce_immutable)
	GEAR_PARSE_PARAMETERS_END();

	hyss_date_instantiate(date_ce_date, return_value);
	old_obj = Z_HYSSDATE_P(datetimeimmutable_object);
	new_obj = Z_HYSSDATE_P(return_value);

	new_obj->time = timelib_time_clone(old_obj->time);
}
/* }}} */

/* {{{ proto DateTimeImmutable::createFromMutable(DateTime object)
   Creates new DateTimeImmutable object from an existing mutable DateTime object.
*/
HYSS_METHOD(DateTimeImmutable, createFromMutable)
{
	zval *datetime_object = NULL;
	hyss_date_obj *new_obj = NULL;
	hyss_date_obj *old_obj = NULL;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_OBJECT_OF_CLASS(datetime_object, date_ce_date)
	GEAR_PARSE_PARAMETERS_END();

	hyss_date_instantiate(date_ce_immutable, return_value);
	old_obj = Z_HYSSDATE_P(datetime_object);
	new_obj = Z_HYSSDATE_P(return_value);

	new_obj->time = timelib_time_clone(old_obj->time);
}
/* }}} */

static int hyss_date_initialize_from_hash(hyss_date_obj **dateobj, HashTable *myht)
{
	zval             *z_date;
	zval              tmp_obj;
	timelib_tzinfo   *tzi;

	z_date = gear_hash_str_find(myht, "date", sizeof("data")-1);
	if (z_date && Z_TYPE_P(z_date) == IS_STRING) {
		zval *z_timezone_type = gear_hash_str_find(myht, "timezone_type", sizeof("timezone_type")-1);
		if (z_timezone_type && Z_TYPE_P(z_timezone_type) == IS_LONG) {
			zval *z_timezone = gear_hash_str_find(myht, "timezone", sizeof("timezone")-1);
			if (z_timezone && Z_TYPE_P(z_timezone) == IS_STRING) {
				switch (Z_LVAL_P(z_timezone_type)) {
					case TIMELIB_ZONETYPE_OFFSET:
					case TIMELIB_ZONETYPE_ABBR: {
						char *tmp = emalloc(Z_STRLEN_P(z_date) + Z_STRLEN_P(z_timezone) + 2);
						int ret;
						snprintf(tmp, Z_STRLEN_P(z_date) + Z_STRLEN_P(z_timezone) + 2, "%s %s", Z_STRVAL_P(z_date), Z_STRVAL_P(z_timezone));
						ret = hyss_date_initialize(*dateobj, tmp, Z_STRLEN_P(z_date) + Z_STRLEN_P(z_timezone) + 1, NULL, NULL, 0);
						efree(tmp);
						return 1 == ret;
					}

					case TIMELIB_ZONETYPE_ID: {
						int ret;
						hyss_timezone_obj *tzobj;

						tzi = hyss_date_parse_tzfile(Z_STRVAL_P(z_timezone), DATE_TIMEZONEDB);

						if (tzi == NULL) {
							return 0;
						}

						tzobj = Z_HYSSTIMEZONE_P(hyss_date_instantiate(date_ce_timezone, &tmp_obj));
						tzobj->type = TIMELIB_ZONETYPE_ID;
						tzobj->tzi.tz = tzi;
						tzobj->initialized = 1;

						ret = hyss_date_initialize(*dateobj, Z_STRVAL_P(z_date), Z_STRLEN_P(z_date), NULL, &tmp_obj, 0);
						zval_ptr_dtor(&tmp_obj);
						return 1 == ret;
					}
				}
			}
		}
	}
	return 0;
} /* }}} */

/* {{{ proto DateTime::__set_state(array array)
*/
HYSS_METHOD(DateTime, __set_state)
{
	hyss_date_obj     *dateobj;
	zval             *array;
	HashTable        *myht;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ARRAY(array)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	myht = Z_ARRVAL_P(array);

	hyss_date_instantiate(date_ce_date, return_value);
	dateobj = Z_HYSSDATE_P(return_value);
	if (!hyss_date_initialize_from_hash(&dateobj, myht)) {
		gear_throw_error(NULL, "Invalid serialization data for DateTime object");
	}
}
/* }}} */

/* {{{ proto DateTimeImmutable::__set_state(array array)
*/
HYSS_METHOD(DateTimeImmutable, __set_state)
{
	hyss_date_obj     *dateobj;
	zval             *array;
	HashTable        *myht;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ARRAY(array)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	myht = Z_ARRVAL_P(array);

	hyss_date_instantiate(date_ce_immutable, return_value);
	dateobj = Z_HYSSDATE_P(return_value);
	if (!hyss_date_initialize_from_hash(&dateobj, myht)) {
		gear_throw_error(NULL, "Invalid serialization data for DateTimeImmutable object");
	}
}
/* }}} */

/* {{{ proto DateTime::__wakeup()
*/
HYSS_METHOD(DateTime, __wakeup)
{
	zval             *object = getThis();
	hyss_date_obj     *dateobj;
	HashTable        *myht;

	dateobj = Z_HYSSDATE_P(object);

	myht = Z_OBJPROP_P(object);

	if (!hyss_date_initialize_from_hash(&dateobj, myht)) {
		gear_throw_error(NULL, "Invalid serialization data for DateTime object");
	}
}
/* }}} */

/* Helper function used to add an associative array of warnings and errors to a zval */
static void zval_from_error_container(zval *z, timelib_error_container *error) /* {{{ */
{
	int   i;
	zval element;

	add_assoc_long(z, "warning_count", error->warning_count);
	array_init(&element);
	for (i = 0; i < error->warning_count; i++) {
		add_index_string(&element, error->warning_messages[i].position, error->warning_messages[i].message);
	}
	add_assoc_zval(z, "warnings", &element);

	add_assoc_long(z, "error_count", error->error_count);
	array_init(&element);
	for (i = 0; i < error->error_count; i++) {
		add_index_string(&element, error->error_messages[i].position, error->error_messages[i].message);
	}
	add_assoc_zval(z, "errors", &element);
} /* }}} */

/* {{{ proto array date_get_last_errors()
   Returns the warnings and errors found while parsing a date/time string.
*/
HYSS_FUNCTION(date_get_last_errors)
{
	if (DATEG(last_errors)) {
		array_init(return_value);
		zval_from_error_container(return_value, DATEG(last_errors));
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

void hyss_date_do_return_parsed_time(INTERNAL_FUNCTION_PARAMETERS, timelib_time *parsed_time, timelib_error_container *error) /* {{{ */
{
	zval element;

	array_init(return_value);
#define HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(name, elem) \
	if (parsed_time->elem == -99999) {               \
		add_assoc_bool(return_value, #name, 0); \
	} else {                                       \
		add_assoc_long(return_value, #name, parsed_time->elem); \
	}
	HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(year,      y);
	HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(month,     m);
	HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(day,       d);
	HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(hour,      h);
	HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(minute,    i);
	HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(second,    s);

	if (parsed_time->us == -99999) {
		add_assoc_bool(return_value, "fraction", 0);
	} else {
		add_assoc_double(return_value, "fraction", (double)parsed_time->us / 1000000.0);
	}

	zval_from_error_container(return_value, error);

	timelib_error_container_dtor(error);

	add_assoc_bool(return_value, "is_localtime", parsed_time->is_localtime);

	if (parsed_time->is_localtime) {
		HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(zone_type, zone_type);
		switch (parsed_time->zone_type) {
			case TIMELIB_ZONETYPE_OFFSET:
				HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(zone, z);
				add_assoc_bool(return_value, "is_dst", parsed_time->dst);
				break;
			case TIMELIB_ZONETYPE_ID:
				if (parsed_time->tz_abbr) {
					add_assoc_string(return_value, "tz_abbr", parsed_time->tz_abbr);
				}
				if (parsed_time->tz_info) {
					add_assoc_string(return_value, "tz_id", parsed_time->tz_info->name);
				}
				break;
			case TIMELIB_ZONETYPE_ABBR:
				HYSS_DATE_PARSE_DATE_SET_TIME_ELEMENT(zone, z);
				add_assoc_bool(return_value, "is_dst", parsed_time->dst);
				add_assoc_string(return_value, "tz_abbr", parsed_time->tz_abbr);
				break;
		}
	}
	if (parsed_time->have_relative) {
		array_init(&element);
		add_assoc_long(&element, "year",   parsed_time->relative.y);
		add_assoc_long(&element, "month",  parsed_time->relative.m);
		add_assoc_long(&element, "day",    parsed_time->relative.d);
		add_assoc_long(&element, "hour",   parsed_time->relative.h);
		add_assoc_long(&element, "minute", parsed_time->relative.i);
		add_assoc_long(&element, "second", parsed_time->relative.s);
		if (parsed_time->relative.have_weekday_relative) {
			add_assoc_long(&element, "weekday", parsed_time->relative.weekday);
		}
		if (parsed_time->relative.have_special_relative && (parsed_time->relative.special.type == TIMELIB_SPECIAL_WEEKDAY)) {
			add_assoc_long(&element, "weekdays", parsed_time->relative.special.amount);
		}
		if (parsed_time->relative.first_last_day_of) {
			add_assoc_bool(&element, parsed_time->relative.first_last_day_of == TIMELIB_SPECIAL_FIRST_DAY_OF_MONTH ? "first_day_of_month" : "last_day_of_month", 1);
		}
		add_assoc_zval(return_value, "relative", &element);
	}
	timelib_time_dtor(parsed_time);
} /* }}} */

/* {{{ proto array date_parse(string date)
   Returns associative array with detailed info about given date
*/
HYSS_FUNCTION(date_parse)
{
	gear_string                    *date;
	timelib_error_container *error;
	timelib_time                   *parsed_time;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(date)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	parsed_time = timelib_strtotime(ZSTR_VAL(date), ZSTR_LEN(date), &error, DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);
	hyss_date_do_return_parsed_time(INTERNAL_FUNCTION_PARAM_PASSTHRU, parsed_time, error);
}
/* }}} */

/* {{{ proto array date_parse_from_format(string format, string date)
   Returns associative array with detailed info about given date
*/
HYSS_FUNCTION(date_parse_from_format)
{
	gear_string                    *date, *format;
	timelib_error_container *error;
	timelib_time                   *parsed_time;

	GEAR_PARSE_PARAMETERS_START(2, 2)
		Z_PARAM_STR(format)
		Z_PARAM_STR(date)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	parsed_time = timelib_parse_from_format(ZSTR_VAL(format), ZSTR_VAL(date), ZSTR_LEN(date), &error, DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);
	hyss_date_do_return_parsed_time(INTERNAL_FUNCTION_PARAM_PASSTHRU, parsed_time, error);
}
/* }}} */

/* {{{ proto string date_format(DateTimeInterface object, string format)
   Returns date formatted according to given format
*/
HYSS_FUNCTION(date_format)
{
	zval         *object;
	hyss_date_obj *dateobj;
	char         *format;
	size_t       format_len;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Os", &object, date_ce_interface, &format, &format_len) == FAILURE) {
		RETURN_FALSE;
	}
	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	RETURN_STR(date_format(format, format_len, dateobj->time, dateobj->time->is_localtime));
}
/* }}} */

static int hyss_date_modify(zval *object, char *modify, size_t modify_len) /* {{{ */
{
	hyss_date_obj *dateobj;
	timelib_time *tmp_time;
	timelib_error_container *err = NULL;

	dateobj = Z_HYSSDATE_P(object);

	if (!(dateobj->time)) {
		hyss_error_docref(NULL, E_WARNING, "The DateTime object has not been correctly initialized by its constructor");
		return 0;
	}

	tmp_time = timelib_strtotime(modify, modify_len, &err, DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);

	/* update last errors and warnings */
	update_errors_warnings(err);
	if (err && err->error_count) {
		/* spit out the first library error message, at least */
		hyss_error_docref(NULL, E_WARNING, "Failed to parse time string (%s) at position %d (%c): %s", modify,
			err->error_messages[0].position, err->error_messages[0].character, err->error_messages[0].message);
		timelib_time_dtor(tmp_time);
		return 0;
	}

	memcpy(&dateobj->time->relative, &tmp_time->relative, sizeof(timelib_rel_time));
	dateobj->time->have_relative = tmp_time->have_relative;
	dateobj->time->sse_uptodate = 0;

	if (tmp_time->y != -99999) {
		dateobj->time->y = tmp_time->y;
	}
	if (tmp_time->m != -99999) {
		dateobj->time->m = tmp_time->m;
	}
	if (tmp_time->d != -99999) {
		dateobj->time->d = tmp_time->d;
	}

	if (tmp_time->h != -99999) {
		dateobj->time->h = tmp_time->h;
		if (tmp_time->i != -99999) {
			dateobj->time->i = tmp_time->i;
			if (tmp_time->s != -99999) {
				dateobj->time->s = tmp_time->s;
			} else {
				dateobj->time->s = 0;
			}
		} else {
			dateobj->time->i = 0;
			dateobj->time->s = 0;
		}
	}

	if (tmp_time->us != -99999) {
		dateobj->time->us = tmp_time->us;
	}

	timelib_time_dtor(tmp_time);

	timelib_update_ts(dateobj->time, NULL);
	timelib_update_from_sse(dateobj->time);
	dateobj->time->have_relative = 0;
	memset(&dateobj->time->relative, 0, sizeof(dateobj->time->relative));

	return 1;
} /* }}} */

/* {{{ proto DateTime date_modify(DateTime object, string modify)
   Alters the timestamp.
*/
HYSS_FUNCTION(date_modify)
{
	zval         *object;
	char         *modify;
	size_t        modify_len;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Os", &object, date_ce_date, &modify, &modify_len) == FAILURE) {
		RETURN_FALSE;
	}

	if (!hyss_date_modify(object, modify, modify_len)) {
		RETURN_FALSE;
	}

	Z_ADDREF_P(object);
	ZVAL_COPY_VALUE(return_value, object);
}
/* }}} */

/* {{{ proto DateTimeImmutable::modify()
*/
HYSS_METHOD(DateTimeImmutable, modify)
{
	zval *object, new_object;
	char *modify;
	size_t   modify_len;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Os", &object, date_ce_immutable, &modify, &modify_len) == FAILURE) {
		RETURN_FALSE;
	}

	date_clone_immutable(object, &new_object);
	if (!hyss_date_modify(&new_object, modify, modify_len)) {
		RETURN_FALSE;
	}

	ZVAL_OBJ(return_value, Z_OBJ(new_object));
}
/* }}} */

static void hyss_date_add(zval *object, zval *interval, zval *return_value) /* {{{ */
{
	hyss_date_obj     *dateobj;
	hyss_interval_obj *intobj;
	timelib_time     *new_time;

	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	intobj = Z_HYSSINTERVAL_P(interval);
	DATE_CHECK_INITIALIZED(intobj->initialized, DateInterval);

	new_time = timelib_add(dateobj->time, intobj->diff);
	timelib_time_dtor(dateobj->time);
	dateobj->time = new_time;
} /* }}} */

/* {{{ proto DateTime date_add(DateTime object, DateInterval interval)
   Adds an interval to the current date in object.
*/
HYSS_FUNCTION(date_add)
{
	zval *object, *interval;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "OO", &object, date_ce_date, &interval, date_ce_interval) == FAILURE) {
		RETURN_FALSE;
	}

	hyss_date_add(object, interval, return_value);

	Z_ADDREF_P(object);
	ZVAL_COPY_VALUE(return_value, object);
}
/* }}} */

/* {{{ proto DateTimeImmutable::add()
*/
HYSS_METHOD(DateTimeImmutable, add)
{
	zval *object, *interval, new_object;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "OO", &object, date_ce_immutable, &interval, date_ce_interval) == FAILURE) {
		RETURN_FALSE;
	}

	date_clone_immutable(object, &new_object);
	hyss_date_add(&new_object, interval, return_value);

	ZVAL_OBJ(return_value, Z_OBJ(new_object));
}
/* }}} */

static void hyss_date_sub(zval *object, zval *interval, zval *return_value) /* {{{ */
{
	hyss_date_obj     *dateobj;
	hyss_interval_obj *intobj;
	timelib_time     *new_time;

	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	intobj = Z_HYSSINTERVAL_P(interval);
	DATE_CHECK_INITIALIZED(intobj->initialized, DateInterval);

	if (intobj->diff->have_special_relative) {
		hyss_error_docref(NULL, E_WARNING, "Only non-special relative time specifications are supported for subtraction");
		return;
	}

	new_time = timelib_sub(dateobj->time, intobj->diff);
	timelib_time_dtor(dateobj->time);
	dateobj->time = new_time;
} /* }}} */

/* {{{ proto DateTime date_sub(DateTime object, DateInterval interval)
   Subtracts an interval to the current date in object.
*/
HYSS_FUNCTION(date_sub)
{
	zval *object, *interval;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "OO", &object, date_ce_date, &interval, date_ce_interval) == FAILURE) {
		RETURN_FALSE;
	}

	hyss_date_sub(object, interval, return_value);

	Z_ADDREF_P(object);
	ZVAL_COPY_VALUE(return_value, object);
}
/* }}} */

/* {{{ proto DateTimeImmutable::sub()
*/
HYSS_METHOD(DateTimeImmutable, sub)
{
	zval *object, *interval, new_object;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "OO", &object, date_ce_immutable, &interval, date_ce_interval) == FAILURE) {
		RETURN_FALSE;
	}

	date_clone_immutable(object, &new_object);
	hyss_date_sub(&new_object, interval, return_value);

	ZVAL_OBJ(return_value, Z_OBJ(new_object));
}
/* }}} */

static void set_timezone_from_timelib_time(hyss_timezone_obj *tzobj, timelib_time *t)
{
       tzobj->initialized = 1;
       tzobj->type = t->zone_type;
       switch (t->zone_type) {
               case TIMELIB_ZONETYPE_ID:
                       tzobj->tzi.tz = t->tz_info;
                       break;
               case TIMELIB_ZONETYPE_OFFSET:
                       tzobj->tzi.utc_offset = t->z;
                       break;
               case TIMELIB_ZONETYPE_ABBR:
                       tzobj->tzi.z.utc_offset = t->z;
                       tzobj->tzi.z.dst = t->dst;
                       tzobj->tzi.z.abbr = timelib_strdup(t->tz_abbr);
                       break;
       }
}


/* {{{ proto DateTimeZone date_timezone_get(DateTimeInterface object)
   Return new DateTimeZone object relative to give DateTime
*/
HYSS_FUNCTION(date_timezone_get)
{
	zval             *object;
	hyss_date_obj     *dateobj;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O", &object, date_ce_interface) == FAILURE) {
		RETURN_FALSE;
	}
	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	if (dateobj->time->is_localtime) {
		hyss_timezone_obj *tzobj;
		hyss_date_instantiate(date_ce_timezone, return_value);
		tzobj = Z_HYSSTIMEZONE_P(return_value);
		set_timezone_from_timelib_time(tzobj, dateobj->time);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

static void hyss_date_timezone_set(zval *object, zval *timezone_object, zval *return_value) /* {{{ */
{
	hyss_date_obj     *dateobj;
	hyss_timezone_obj *tzobj;

	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	tzobj = Z_HYSSTIMEZONE_P(timezone_object);

	switch (tzobj->type) {
		case TIMELIB_ZONETYPE_OFFSET:
			timelib_set_timezone_from_offset(dateobj->time, tzobj->tzi.utc_offset);
			break;
		case TIMELIB_ZONETYPE_ABBR:
			timelib_set_timezone_from_abbr(dateobj->time, tzobj->tzi.z);
			break;
		case TIMELIB_ZONETYPE_ID:
			timelib_set_timezone(dateobj->time, tzobj->tzi.tz);
			break;
	}
	timelib_unixtime2local(dateobj->time, dateobj->time->sse);
} /* }}} */

/* {{{ proto DateTime date_timezone_set(DateTime object, DateTimeZone object)
   Sets the timezone for the DateTime object.
*/
HYSS_FUNCTION(date_timezone_set)
{
	zval *object;
	zval *timezone_object;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "OO", &object, date_ce_date, &timezone_object, date_ce_timezone) == FAILURE) {
		RETURN_FALSE;
	}

	hyss_date_timezone_set(object, timezone_object, return_value);

	Z_ADDREF_P(object);
	ZVAL_COPY_VALUE(return_value, object);
}
/* }}} */

/* {{{ proto DateTimeImmutable::setTimezone()
*/
HYSS_METHOD(DateTimeImmutable, setTimezone)
{
	zval *object, new_object;
	zval *timezone_object;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "OO", &object, date_ce_immutable, &timezone_object, date_ce_timezone) == FAILURE) {
		RETURN_FALSE;
	}

	date_clone_immutable(object, &new_object);
	hyss_date_timezone_set(&new_object, timezone_object, return_value);

	ZVAL_OBJ(return_value, Z_OBJ(new_object));
}
/* }}} */

/* {{{ proto int date_offset_get(DateTimeInterface object)
   Returns the DST offset.
*/
HYSS_FUNCTION(date_offset_get)
{
	zval                *object;
	hyss_date_obj        *dateobj;
	timelib_time_offset *offset;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O", &object, date_ce_interface) == FAILURE) {
		RETURN_FALSE;
	}
	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	if (dateobj->time->is_localtime) {
		switch (dateobj->time->zone_type) {
			case TIMELIB_ZONETYPE_ID:
				offset = timelib_get_time_zone_info(dateobj->time->sse, dateobj->time->tz_info);
				RETVAL_LONG(offset->offset);
				timelib_time_offset_dtor(offset);
				break;
			case TIMELIB_ZONETYPE_OFFSET:
				RETVAL_LONG(dateobj->time->z);
				break;
			case TIMELIB_ZONETYPE_ABBR:
				RETVAL_LONG((dateobj->time->z + (3600 * dateobj->time->dst)));
				break;
		}
		return;
	} else {
		RETURN_LONG(0);
	}
}
/* }}} */

static void hyss_date_time_set(zval *object, gear_long h, gear_long i, gear_long s, gear_long ms, zval *return_value) /* {{{ */
{
	hyss_date_obj *dateobj;

	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	dateobj->time->h = h;
	dateobj->time->i = i;
	dateobj->time->s = s;
	dateobj->time->us = ms;
	timelib_update_ts(dateobj->time, NULL);
} /* }}} */

/* {{{ proto DateTime date_time_set(DateTime object, int hour, int minute[, int second[, int microseconds]])
   Sets the time.
*/
HYSS_FUNCTION(date_time_set)
{
	zval *object;
	gear_long  h, i, s = 0, ms = 0;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oll|ll", &object, date_ce_date, &h, &i, &s, &ms) == FAILURE) {
		RETURN_FALSE;
	}

	hyss_date_time_set(object, h, i, s, ms, return_value);

	Z_ADDREF_P(object);
	ZVAL_COPY_VALUE(return_value, object);
}
/* }}} */

/* {{{ proto DateTimeImmutable::setTime()
*/
HYSS_METHOD(DateTimeImmutable, setTime)
{
	zval *object, new_object;
	gear_long  h, i, s = 0, ms = 0;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oll|ll", &object, date_ce_immutable, &h, &i, &s, &ms) == FAILURE) {
		RETURN_FALSE;
	}

	date_clone_immutable(object, &new_object);
	hyss_date_time_set(&new_object, h, i, s, ms, return_value);

	ZVAL_OBJ(return_value, Z_OBJ(new_object));
}
/* }}} */

static void hyss_date_date_set(zval *object, gear_long y, gear_long m, gear_long d, zval *return_value) /* {{{ */
{
	hyss_date_obj *dateobj;

	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	dateobj->time->y = y;
	dateobj->time->m = m;
	dateobj->time->d = d;
	timelib_update_ts(dateobj->time, NULL);
} /* }}} */

/* {{{ proto DateTime date_date_set(DateTime object, int year, int month, int day)
   Sets the date.
*/
HYSS_FUNCTION(date_date_set)
{
	zval *object;
	gear_long  y, m, d;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Olll", &object, date_ce_date, &y, &m, &d) == FAILURE) {
		RETURN_FALSE;
	}

	hyss_date_date_set(object, y, m, d, return_value);

	Z_ADDREF_P(object);
	ZVAL_COPY_VALUE(return_value, object);
}
/* }}} */

/* {{{ proto DateTimeImmutable::setDate()
*/
HYSS_METHOD(DateTimeImmutable, setDate)
{
	zval *object, new_object;
	gear_long  y, m, d;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Olll", &object, date_ce_immutable, &y, &m, &d) == FAILURE) {
		RETURN_FALSE;
	}

	date_clone_immutable(object, &new_object);
	hyss_date_date_set(&new_object, y, m, d, return_value);

	ZVAL_OBJ(return_value, Z_OBJ(new_object));
}
/* }}} */

static void hyss_date_isodate_set(zval *object, gear_long y, gear_long w, gear_long d, zval *return_value) /* {{{ */
{
	hyss_date_obj *dateobj;

	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	dateobj->time->y = y;
	dateobj->time->m = 1;
	dateobj->time->d = 1;
	memset(&dateobj->time->relative, 0, sizeof(dateobj->time->relative));
	dateobj->time->relative.d = timelib_daynr_from_weeknr(y, w, d);
	dateobj->time->have_relative = 1;

	timelib_update_ts(dateobj->time, NULL);
} /* }}} */

/* {{{ proto DateTime date_isodate_set(DateTime object, int year, int week[, int day])
   Sets the ISO date.
*/
HYSS_FUNCTION(date_isodate_set)
{
	zval *object;
	gear_long  y, w, d = 1;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oll|l", &object, date_ce_date, &y, &w, &d) == FAILURE) {
		RETURN_FALSE;
	}

	hyss_date_isodate_set(object, y, w, d, return_value);

	Z_ADDREF_P(object);
	ZVAL_COPY_VALUE(return_value, object);
}
/* }}} */

/* {{{ proto DateTimeImmutable::setISODate()
*/
HYSS_METHOD(DateTimeImmutable, setISODate)
{
	zval *object, new_object;
	gear_long  y, w, d = 1;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Oll|l", &object, date_ce_immutable, &y, &w, &d) == FAILURE) {
		RETURN_FALSE;
	}

	date_clone_immutable(object, &new_object);
	hyss_date_isodate_set(&new_object, y, w, d, return_value);

	ZVAL_OBJ(return_value, Z_OBJ(new_object));
}
/* }}} */

static void hyss_date_timestamp_set(zval *object, gear_long timestamp, zval *return_value) /* {{{ */
{
	hyss_date_obj *dateobj;

	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	timelib_unixtime2local(dateobj->time, (timelib_sll)timestamp);
	timelib_update_ts(dateobj->time, NULL);
	hyss_date_set_time_fraction(dateobj->time, 0);
} /* }}} */

/* {{{ proto DateTime date_timestamp_set(DateTime object, int unixTimestamp)
   Sets the date and time based on an Unix timestamp.
*/
HYSS_FUNCTION(date_timestamp_set)
{
	zval *object;
	gear_long  timestamp;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Ol", &object, date_ce_date, &timestamp) == FAILURE) {
		RETURN_FALSE;
	}

	hyss_date_timestamp_set(object, timestamp, return_value);

	Z_ADDREF_P(object);
	ZVAL_COPY_VALUE(return_value, object);
}
/* }}} */

/* {{{ proto DateTimeImmutable::setTimestamp()
*/
HYSS_METHOD(DateTimeImmutable, setTimestamp)
{
	zval *object, new_object;
	gear_long  timestamp;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Ol", &object, date_ce_immutable, &timestamp) == FAILURE) {
		RETURN_FALSE;
	}

	date_clone_immutable(object, &new_object);
	hyss_date_timestamp_set(&new_object, timestamp, return_value);

	ZVAL_OBJ(return_value, Z_OBJ(new_object));
}
/* }}} */

/* {{{ proto int date_timestamp_get(DateTimeInterface object)
   Gets the Unix timestamp.
*/
HYSS_FUNCTION(date_timestamp_get)
{
	zval         *object;
	hyss_date_obj *dateobj;
	gear_long          timestamp;
	int           error;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O", &object, date_ce_interface) == FAILURE) {
		RETURN_FALSE;
	}
	dateobj = Z_HYSSDATE_P(object);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTime);
	timelib_update_ts(dateobj->time, NULL);

	timestamp = timelib_date_to_int(dateobj->time, &error);
	if (error) {
		RETURN_FALSE;
	} else {
		RETVAL_LONG(timestamp);
	}
}
/* }}} */

/* {{{ proto DateInterval date_diff(DateTime object [, bool absolute])
   Returns the difference between two DateTime objects.
*/
HYSS_FUNCTION(date_diff)
{
	zval         *object1, *object2;
	hyss_date_obj *dateobj1, *dateobj2;
	hyss_interval_obj *interval;
	gear_bool      absolute = 0;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "OO|b", &object1, date_ce_interface, &object2, date_ce_interface, &absolute) == FAILURE) {
		RETURN_FALSE;
	}
	dateobj1 = Z_HYSSDATE_P(object1);
	dateobj2 = Z_HYSSDATE_P(object2);
	DATE_CHECK_INITIALIZED(dateobj1->time, DateTimeInterface);
	DATE_CHECK_INITIALIZED(dateobj2->time, DateTimeInterface);
	timelib_update_ts(dateobj1->time, NULL);
	timelib_update_ts(dateobj2->time, NULL);

	hyss_date_instantiate(date_ce_interval, return_value);
	interval = Z_HYSSINTERVAL_P(return_value);
	interval->diff = timelib_diff(dateobj1->time, dateobj2->time);
	if (absolute) {
		interval->diff->invert = 0;
	}
	interval->initialized = 1;
}
/* }}} */

static int timezone_initialize(hyss_timezone_obj *tzobj, /*const*/ char *tz, size_t tz_len) /* {{{ */
{
	timelib_time *dummy_t = ecalloc(1, sizeof(timelib_time));
	int           dst, not_found;
	char         *orig_tz = tz;

	if (strlen(tz) != tz_len) {
		hyss_error_docref(NULL, E_WARNING, "Timezone must not contain null bytes");
		efree(dummy_t);
		return FAILURE;
	}

	dummy_t->z = timelib_parse_zone(&tz, &dst, dummy_t, &not_found, DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);
	if (not_found) {
		hyss_error_docref(NULL, E_WARNING, "Unknown or bad timezone (%s)", orig_tz);
		efree(dummy_t);
		return FAILURE;
	} else {
		set_timezone_from_timelib_time(tzobj, dummy_t);
		timelib_free(dummy_t->tz_abbr);
		efree(dummy_t);
		return SUCCESS;
	}
} /* }}} */

/* {{{ proto DateTimeZone timezone_open(string timezone)
   Returns new DateTimeZone object
*/
HYSS_FUNCTION(timezone_open)
{
	gear_string *tz;
	hyss_timezone_obj *tzobj;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(tz)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	tzobj = Z_HYSSTIMEZONE_P(hyss_date_instantiate(date_ce_timezone, return_value));
	if (SUCCESS != timezone_initialize(tzobj, ZSTR_VAL(tz), ZSTR_LEN(tz))) {
		zval_ptr_dtor(return_value);
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto DateTimeZone::__construct(string timezone)
   Creates new DateTimeZone object.
*/
HYSS_METHOD(DateTimeZone, __construct)
{
	gear_string *tz;
	hyss_timezone_obj *tzobj;
	gear_error_handling error_handling;

	GEAR_PARSE_PARAMETERS_START_EX(GEAR_PARSE_PARAMS_THROW, 1, 1)
		Z_PARAM_STR(tz)
	GEAR_PARSE_PARAMETERS_END();

	gear_replace_error_handling(EH_THROW, NULL, &error_handling);
	tzobj = Z_HYSSTIMEZONE_P(getThis());
	timezone_initialize(tzobj, ZSTR_VAL(tz), ZSTR_LEN(tz));
	gear_restore_error_handling(&error_handling);
}
/* }}} */

static int hyss_date_timezone_initialize_from_hash(zval **return_value, hyss_timezone_obj **tzobj, HashTable *myht) /* {{{ */
{
	zval            *z_timezone_type;

	if ((z_timezone_type = gear_hash_str_find(myht, "timezone_type", sizeof("timezone_type") - 1)) != NULL) {
		zval *z_timezone;

		if ((z_timezone = gear_hash_str_find(myht, "timezone", sizeof("timezone") - 1)) != NULL) {
			if (Z_TYPE_P(z_timezone_type) != IS_LONG) {
				return FAILURE;
			}
			if (Z_TYPE_P(z_timezone) != IS_STRING) {
				return FAILURE;
			}
			if (SUCCESS == timezone_initialize(*tzobj, Z_STRVAL_P(z_timezone), Z_STRLEN_P(z_timezone))) {
				return SUCCESS;
			}
		}
	}
	return FAILURE;
} /* }}} */

/* {{{ proto DateTimeZone::__set_state(array array)
 *  */
HYSS_METHOD(DateTimeZone, __set_state)
{
	hyss_timezone_obj *tzobj;
	zval             *array;
	HashTable        *myht;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ARRAY(array)
	GEAR_PARSE_PARAMETERS_END();

	myht = Z_ARRVAL_P(array);

	hyss_date_instantiate(date_ce_timezone, return_value);
	tzobj = Z_HYSSTIMEZONE_P(return_value);
	if(hyss_date_timezone_initialize_from_hash(&return_value, &tzobj, myht) != SUCCESS) {
		gear_throw_error(NULL, "Timezone initialization failed");
		zval_ptr_dtor(return_value);
	}
}
/* }}} */

/* {{{ proto DateTimeZone::__wakeup()
 *  */
HYSS_METHOD(DateTimeZone, __wakeup)
{
	zval             *object = getThis();
	hyss_timezone_obj *tzobj;
	HashTable        *myht;

	tzobj = Z_HYSSTIMEZONE_P(object);

	myht = Z_OBJPROP_P(object);

	if(hyss_date_timezone_initialize_from_hash(&return_value, &tzobj, myht) != SUCCESS) {
		gear_throw_error(NULL, "Timezone initialization failed");
	}
}
/* }}} */

/* {{{ proto string timezone_name_get(DateTimeZone object)
   Returns the name of the timezone.
*/
HYSS_FUNCTION(timezone_name_get)
{
	zval             *object;
	hyss_timezone_obj *tzobj;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O", &object, date_ce_timezone) == FAILURE) {
		RETURN_FALSE;
	}
	tzobj = Z_HYSSTIMEZONE_P(object);
	DATE_CHECK_INITIALIZED(tzobj->initialized, DateTimeZone);
	hyss_timezone_to_string(tzobj, return_value);
}
/* }}} */

/* {{{ proto string timezone_name_from_abbr(string abbr[, int gmtOffset[, int isdst]])
   Returns the timezone name from abbrevation
*/
HYSS_FUNCTION(timezone_name_from_abbr)
{
	gear_string  *abbr;
	char         *tzid;
	gear_long     gmtoffset = -1;
	gear_long     isdst = -1;

	GEAR_PARSE_PARAMETERS_START(1, 3)
		Z_PARAM_STR(abbr)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(gmtoffset)
		Z_PARAM_LONG(isdst)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	tzid = timelib_timezone_id_from_abbr(ZSTR_VAL(abbr), gmtoffset, isdst);

	if (tzid) {
		RETURN_STRING(tzid);
	} else {
		RETURN_FALSE;
	}
}
/* }}} */

/* {{{ proto int timezone_offset_get(DateTimeZone object, DateTimeInterface datetime)
   Returns the timezone offset.
*/
HYSS_FUNCTION(timezone_offset_get)
{
	zval                *object, *dateobject;
	hyss_timezone_obj    *tzobj;
	hyss_date_obj        *dateobj;
	timelib_time_offset *offset;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "OO", &object, date_ce_timezone, &dateobject, date_ce_interface) == FAILURE) {
		RETURN_FALSE;
	}
	tzobj = Z_HYSSTIMEZONE_P(object);
	DATE_CHECK_INITIALIZED(tzobj->initialized, DateTimeZone);
	dateobj = Z_HYSSDATE_P(dateobject);
	DATE_CHECK_INITIALIZED(dateobj->time, DateTimeInterface);

	switch (tzobj->type) {
		case TIMELIB_ZONETYPE_ID:
			offset = timelib_get_time_zone_info(dateobj->time->sse, tzobj->tzi.tz);
			RETVAL_LONG(offset->offset);
			timelib_time_offset_dtor(offset);
			break;
		case TIMELIB_ZONETYPE_OFFSET:
			RETURN_LONG(tzobj->tzi.utc_offset);
			break;
		case TIMELIB_ZONETYPE_ABBR:
			RETURN_LONG(tzobj->tzi.z.utc_offset + (tzobj->tzi.z.dst * 3600));
			break;
	}
}
/* }}} */

/* {{{ proto array timezone_transitions_get(DateTimeZone object [, int timestamp_begin [, int timestamp_end ]])
   Returns numerically indexed array containing associative array for all transitions in the specified range for the timezone.
*/
HYSS_FUNCTION(timezone_transitions_get)
{
	zval                *object, element;
	hyss_timezone_obj    *tzobj;
	unsigned int         begin = 0, found;
	gear_long            timestamp_begin = GEAR_LONG_MIN, timestamp_end = GEAR_LONG_MAX;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O|ll", &object, date_ce_timezone, &timestamp_begin, &timestamp_end) == FAILURE) {
		RETURN_FALSE;
	}
	tzobj = Z_HYSSTIMEZONE_P(object);
	DATE_CHECK_INITIALIZED(tzobj->initialized, DateTimeZone);
	if (tzobj->type != TIMELIB_ZONETYPE_ID) {
		RETURN_FALSE;
	}

#define add_nominal() \
		array_init(&element); \
		add_assoc_long(&element, "ts",     timestamp_begin); \
		add_assoc_str(&element, "time", hyss_format_date(DATE_FORMAT_ISO8601, 13, timestamp_begin, 0)); \
		add_assoc_long(&element, "offset", tzobj->tzi.tz->type[0].offset); \
		add_assoc_bool(&element, "isdst",  tzobj->tzi.tz->type[0].isdst); \
		add_assoc_string(&element, "abbr", &tzobj->tzi.tz->timezone_abbr[tzobj->tzi.tz->type[0].abbr_idx]); \
		add_next_index_zval(return_value, &element);

#define add(i,ts) \
		array_init(&element); \
		add_assoc_long(&element, "ts",     ts); \
		add_assoc_str(&element, "time", hyss_format_date(DATE_FORMAT_ISO8601, 13, ts, 0)); \
		add_assoc_long(&element, "offset", tzobj->tzi.tz->type[tzobj->tzi.tz->trans_idx[i]].offset); \
		add_assoc_bool(&element, "isdst",  tzobj->tzi.tz->type[tzobj->tzi.tz->trans_idx[i]].isdst); \
		add_assoc_string(&element, "abbr", &tzobj->tzi.tz->timezone_abbr[tzobj->tzi.tz->type[tzobj->tzi.tz->trans_idx[i]].abbr_idx]); \
		add_next_index_zval(return_value, &element);

#define add_last() add(tzobj->tzi.tz->bit64.timecnt - 1, timestamp_begin)

	array_init(return_value);

	if (timestamp_begin == GEAR_LONG_MIN) {
		add_nominal();
		begin = 0;
		found = 1;
	} else {
		begin = 0;
		found = 0;
		if (tzobj->tzi.tz->bit64.timecnt > 0) {
			do {
				if (tzobj->tzi.tz->trans[begin] > timestamp_begin) {
					if (begin > 0) {
						add(begin - 1, timestamp_begin);
					} else {
						add_nominal();
					}
					found = 1;
					break;
				}
				begin++;
			} while (begin < tzobj->tzi.tz->bit64.timecnt);
		}
	}

	if (!found) {
		if (tzobj->tzi.tz->bit64.timecnt > 0) {
			add_last();
		} else {
			add_nominal();
		}
	} else {
		unsigned int i;
		for (i = begin; i < tzobj->tzi.tz->bit64.timecnt; ++i) {
			if (tzobj->tzi.tz->trans[i] < timestamp_end) {
				add(i, tzobj->tzi.tz->trans[i]);
			}
		}
	}
}
/* }}} */

/* {{{ proto array timezone_location_get()
   Returns location information for a timezone, including country code, latitude/longitude and comments
*/
HYSS_FUNCTION(timezone_location_get)
{
	zval                *object;
	hyss_timezone_obj    *tzobj;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "O", &object, date_ce_timezone) == FAILURE) {
		RETURN_FALSE;
	}
	tzobj = Z_HYSSTIMEZONE_P(object);
	DATE_CHECK_INITIALIZED(tzobj->initialized, DateTimeZone);
	if (tzobj->type != TIMELIB_ZONETYPE_ID) {
		RETURN_FALSE;
	}

	array_init(return_value);
	add_assoc_string(return_value, "country_code", tzobj->tzi.tz->location.country_code);
	add_assoc_double(return_value, "latitude", tzobj->tzi.tz->location.latitude);
	add_assoc_double(return_value, "longitude", tzobj->tzi.tz->location.longitude);
	add_assoc_string(return_value, "comments", tzobj->tzi.tz->location.comments);
}
/* }}} */

static int date_interval_initialize(timelib_rel_time **rt, /*const*/ char *format, size_t format_length) /* {{{ */
{
	timelib_time     *b = NULL, *e = NULL;
	timelib_rel_time *p = NULL;
	int               r = 0;
	int               retval = 0;
	timelib_error_container *errors;

	timelib_strtointerval(format, format_length, &b, &e, &p, &r, &errors);

	if (errors->error_count > 0) {
		hyss_error_docref(NULL, E_WARNING, "Unknown or bad format (%s)", format);
		retval = FAILURE;
	} else {
		if(p) {
			*rt = p;
			retval = SUCCESS;
		} else {
			if(b && e) {
				timelib_update_ts(b, NULL);
				timelib_update_ts(e, NULL);
				*rt = timelib_diff(b, e);
				retval = SUCCESS;
			} else {
				hyss_error_docref(NULL, E_WARNING, "Failed to parse interval (%s)", format);
				retval = FAILURE;
			}
		}
	}
	timelib_error_container_dtor(errors);
	timelib_free(b);
	timelib_free(e);
	return retval;
} /* }}} */

/* {{{ date_interval_read_property */
zval *date_interval_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv)
{
	hyss_interval_obj *obj;
	zval *retval;
	zval tmp_member;
	timelib_sll value = -1;
	double      fvalue = -1;

 	if (Z_TYPE_P(member) != IS_STRING) {
		ZVAL_STR(&tmp_member, zval_get_string_func(member));
		member = &tmp_member;
		cache_slot = NULL;
	}

	obj = Z_HYSSINTERVAL_P(object);

	if (!obj->initialized) {
		retval = gear_std_read_property(object, member, type, cache_slot, rv);
		if (member == &tmp_member) {
			zval_ptr_dtor_str(&tmp_member);
		}
		return retval;
	}

#define GET_VALUE_FROM_STRUCT(n,m)            \
	if (strcmp(Z_STRVAL_P(member), m) == 0) { \
		value = obj->diff->n;                 \
		break;								  \
	}
	do {
		GET_VALUE_FROM_STRUCT(y, "y");
		GET_VALUE_FROM_STRUCT(m, "m");
		GET_VALUE_FROM_STRUCT(d, "d");
		GET_VALUE_FROM_STRUCT(h, "h");
		GET_VALUE_FROM_STRUCT(i, "i");
		GET_VALUE_FROM_STRUCT(s, "s");
		if (strcmp(Z_STRVAL_P(member), "f") == 0) {
			fvalue = obj->diff->us / 1000000.0;
			break;
		}
		GET_VALUE_FROM_STRUCT(invert, "invert");
		GET_VALUE_FROM_STRUCT(days, "days");
		/* didn't find any */
		retval = gear_std_read_property(object, member, type, cache_slot, rv);

		if (member == &tmp_member) {
			zval_ptr_dtor_str(&tmp_member);
		}

		return retval;
	} while(0);

	retval = rv;

	if (fvalue != -1) {
		ZVAL_DOUBLE(retval, fvalue);
	} else if (value != -99999) {
		ZVAL_LONG(retval, value);
	} else {
		ZVAL_FALSE(retval);
	}

	if (member == &tmp_member) {
		zval_ptr_dtor_str(&tmp_member);
	}

	return retval;
}
/* }}} */

/* {{{ date_interval_write_property */
void date_interval_write_property(zval *object, zval *member, zval *value, void **cache_slot)
{
	hyss_interval_obj *obj;
	zval tmp_member;

 	if (Z_TYPE_P(member) != IS_STRING) {
		ZVAL_STR(&tmp_member, zval_get_string_func(member));
		member = &tmp_member;
		cache_slot = NULL;
	}

	obj = Z_HYSSINTERVAL_P(object);

	if (!obj->initialized) {
		gear_std_write_property(object, member, value, cache_slot);
		if (member == &tmp_member) {
			zval_ptr_dtor_str(&tmp_member);
		}
		return;
	}

#define SET_VALUE_FROM_STRUCT(n,m)            \
	if (strcmp(Z_STRVAL_P(member), m) == 0) { \
		obj->diff->n = zval_get_long(value); \
		break;								  \
	}

	do {
		SET_VALUE_FROM_STRUCT(y, "y");
		SET_VALUE_FROM_STRUCT(m, "m");
		SET_VALUE_FROM_STRUCT(d, "d");
		SET_VALUE_FROM_STRUCT(h, "h");
		SET_VALUE_FROM_STRUCT(i, "i");
		SET_VALUE_FROM_STRUCT(s, "s");
		if (strcmp(Z_STRVAL_P(member), "f") == 0) {
			obj->diff->us = zval_get_double(value) * 1000000;
			break;
		}
		SET_VALUE_FROM_STRUCT(invert, "invert");
		/* didn't find any */
		gear_std_write_property(object, member, value, cache_slot);
	} while(0);

	if (member == &tmp_member) {
		zval_ptr_dtor_str(&tmp_member);
	}
}
/* }}} */

/* {{{ date_interval_get_property_ptr_ptr */
static zval *date_interval_get_property_ptr_ptr(zval *object, zval *member, int type, void **cache_slot)
{
	zval tmp_member, *ret;

	if (Z_TYPE_P(member) != IS_STRING) {
		ZVAL_STR(&tmp_member, zval_get_string_func(member));
		member = &tmp_member;
		cache_slot = NULL;
	}

	if(gear_binary_strcmp("y", sizeof("y") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0 ||
		gear_binary_strcmp("m", sizeof("m") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0 ||
		gear_binary_strcmp("d", sizeof("d") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0 ||
		gear_binary_strcmp("h", sizeof("h") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0 ||
		gear_binary_strcmp("i", sizeof("i") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0 ||
		gear_binary_strcmp("s", sizeof("s") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0 ||
		gear_binary_strcmp("f", sizeof("f") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0 ||
		gear_binary_strcmp("days", sizeof("days") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0 ||
		gear_binary_strcmp("invert", sizeof("invert") - 1, Z_STRVAL_P(member), Z_STRLEN_P(member)) == 0) {
		/* Fallback to read_property. */
		ret = NULL;
	} else {
		ret = gear_std_get_property_ptr_ptr(object, member, type, cache_slot);
	}

	if (member == &tmp_member) {
		zval_ptr_dtor_str(&tmp_member);
	}

	return ret;
}
/* }}} */

/* {{{ proto DateInterval::__construct([string interval_spec])
   Creates new DateInterval object.
*/
HYSS_METHOD(DateInterval, __construct)
{
	gear_string *interval_string = NULL;
	timelib_rel_time *reltime;
	gear_error_handling error_handling;

	GEAR_PARSE_PARAMETERS_START_EX(GEAR_PARSE_PARAMS_THROW, 1, 1)
		Z_PARAM_STR(interval_string)
	GEAR_PARSE_PARAMETERS_END();

	gear_replace_error_handling(EH_THROW, NULL, &error_handling);
	if (date_interval_initialize(&reltime, ZSTR_VAL(interval_string), ZSTR_LEN(interval_string)) == SUCCESS) {
		hyss_interval_obj *diobj = Z_HYSSINTERVAL_P(getThis());
		diobj->diff = reltime;
		diobj->initialized = 1;
	}
	gear_restore_error_handling(&error_handling);
}
/* }}} */


static int hyss_date_interval_initialize_from_hash(zval **return_value, hyss_interval_obj **intobj, HashTable *myht) /* {{{ */
{
	(*intobj)->diff = timelib_rel_time_ctor();

#define HYSS_DATE_INTERVAL_READ_PROPERTY(element, member, itype, def) \
	do { \
		zval *z_arg = gear_hash_str_find(myht, element, sizeof(element) - 1); \
		if (z_arg && Z_TYPE_P(z_arg) <= IS_STRING) { \
			(*intobj)->diff->member = (itype)zval_get_long(z_arg); \
		} else { \
			(*intobj)->diff->member = (itype)def; \
		} \
	} while (0);

#define HYSS_DATE_INTERVAL_READ_PROPERTY_I64(element, member) \
	do { \
		zval *z_arg = gear_hash_str_find(myht, element, sizeof(element) - 1); \
		if (z_arg && Z_TYPE_P(z_arg) <= IS_STRING) { \
			gear_string *tmp_str; \
			gear_string *str = zval_get_tmp_string(z_arg, &tmp_str); \
			DATE_A64I((*intobj)->diff->member, ZSTR_VAL(str)); \
			gear_tmp_string_release(tmp_str); \
		} else { \
			(*intobj)->diff->member = -1LL; \
		} \
	} while (0);

#define HYSS_DATE_INTERVAL_READ_PROPERTY_DOUBLE(element, member, def) \
	do { \
		zval *z_arg = gear_hash_str_find(myht, element, sizeof(element) - 1); \
		if (z_arg) { \
			(*intobj)->diff->member = (double)zval_get_double(z_arg); \
		} else { \
			(*intobj)->diff->member = (double)def; \
		} \
	} while (0);

	HYSS_DATE_INTERVAL_READ_PROPERTY("y", y, timelib_sll, -1)
	HYSS_DATE_INTERVAL_READ_PROPERTY("m", m, timelib_sll, -1)
	HYSS_DATE_INTERVAL_READ_PROPERTY("d", d, timelib_sll, -1)
	HYSS_DATE_INTERVAL_READ_PROPERTY("h", h, timelib_sll, -1)
	HYSS_DATE_INTERVAL_READ_PROPERTY("i", i, timelib_sll, -1)
	HYSS_DATE_INTERVAL_READ_PROPERTY("s", s, timelib_sll, -1)
	do {
		zval *z_arg = gear_hash_str_find(myht, "f", sizeof("f") - 1);
		if (z_arg) {
			(*intobj)->diff->us = ((double)zval_get_double(z_arg) * 1000000);
		} else {
			(*intobj)->diff->us = (double) -1000000;
		}
	} while (0);
	HYSS_DATE_INTERVAL_READ_PROPERTY("weekday", weekday, int, -1)
	HYSS_DATE_INTERVAL_READ_PROPERTY("weekday_behavior", weekday_behavior, int, -1)
	HYSS_DATE_INTERVAL_READ_PROPERTY("first_last_day_of", first_last_day_of, int, -1)
	HYSS_DATE_INTERVAL_READ_PROPERTY("invert", invert, int, 0);
	HYSS_DATE_INTERVAL_READ_PROPERTY_I64("days", days);
	HYSS_DATE_INTERVAL_READ_PROPERTY("special_type", special.type, unsigned int, 0);
	HYSS_DATE_INTERVAL_READ_PROPERTY_I64("special_amount", special.amount);
	HYSS_DATE_INTERVAL_READ_PROPERTY("have_weekday_relative", have_weekday_relative, unsigned int, 0);
	HYSS_DATE_INTERVAL_READ_PROPERTY("have_special_relative", have_special_relative, unsigned int, 0);
	(*intobj)->initialized = 1;

	return 0;
} /* }}} */

/* {{{ proto DateInterval::__set_state(array array)
*/
HYSS_METHOD(DateInterval, __set_state)
{
	hyss_interval_obj *intobj;
	zval             *array;
	HashTable        *myht;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ARRAY(array)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	myht = Z_ARRVAL_P(array);

	hyss_date_instantiate(date_ce_interval, return_value);
	intobj = Z_HYSSINTERVAL_P(return_value);
	hyss_date_interval_initialize_from_hash(&return_value, &intobj, myht);
}
/* }}} */

/* {{{ proto DateInterval::__wakeup()
*/
HYSS_METHOD(DateInterval, __wakeup)
{
	zval             *object = getThis();
	hyss_interval_obj *intobj;
	HashTable        *myht;

	intobj = Z_HYSSINTERVAL_P(object);

	myht = Z_OBJPROP_P(object);

	hyss_date_interval_initialize_from_hash(&return_value, &intobj, myht);
}
/* }}} */

/* {{{ proto DateInterval date_interval_create_from_date_string(string time)
   Uses the normal date parsers and sets up a DateInterval from the relative parts of the parsed string
*/
HYSS_FUNCTION(date_interval_create_from_date_string)
{
	gear_string    *time_str = NULL;
	timelib_time   *time;
	timelib_error_container *err = NULL;
	hyss_interval_obj *diobj;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STR(time_str)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	time = timelib_strtotime(ZSTR_VAL(time_str), ZSTR_LEN(time_str), &err, DATE_TIMEZONEDB, hyss_date_parse_tzfile_wrapper);

	if (err->error_count > 0)  {
		hyss_error_docref(NULL, E_WARNING, "Unknown or bad format (%s) at position %d (%c): %s", ZSTR_VAL(time_str),
			err->error_messages[0].position, err->error_messages[0].character ? err->error_messages[0].character : ' ', err->error_messages[0].message);
		RETVAL_FALSE;
		goto cleanup;
	}

	hyss_date_instantiate(date_ce_interval, return_value);
	diobj = Z_HYSSINTERVAL_P(return_value);
	diobj->diff = timelib_rel_time_clone(&time->relative);
	diobj->initialized = 1;

cleanup:
	timelib_time_dtor(time);
	timelib_error_container_dtor(err);
}
/* }}} */

/* {{{ date_interval_format -  */
static gear_string *date_interval_format(char *format, size_t format_len, timelib_rel_time *t)
{
	smart_str            string = {0};
	size_t               i;
	int                  length, have_format_spec = 0;
	char                 buffer[33];

	if (!format_len) {
		return ZSTR_EMPTY_ALLOC();
	}

	for (i = 0; i < format_len; i++) {
		if (have_format_spec) {
			switch (format[i]) {
				case 'Y': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->y); break;
				case 'y': length = slprintf(buffer, sizeof(buffer), "%d", (int) t->y); break;

				case 'M': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->m); break;
				case 'm': length = slprintf(buffer, sizeof(buffer), "%d", (int) t->m); break;

				case 'D': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->d); break;
				case 'd': length = slprintf(buffer, sizeof(buffer), "%d", (int) t->d); break;

				case 'H': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->h); break;
				case 'h': length = slprintf(buffer, sizeof(buffer), "%d", (int) t->h); break;

				case 'I': length = slprintf(buffer, sizeof(buffer), "%02d", (int) t->i); break;
				case 'i': length = slprintf(buffer, sizeof(buffer), "%d", (int) t->i); break;

				case 'S': length = slprintf(buffer, sizeof(buffer), "%02" GEAR_LONG_FMT_SPEC, (gear_long) t->s); break;
				case 's': length = slprintf(buffer, sizeof(buffer), GEAR_LONG_FMT, (gear_long) t->s); break;

				case 'F': length = slprintf(buffer, sizeof(buffer), "%06" GEAR_LONG_FMT_SPEC, (gear_long) t->us); break;
				case 'f': length = slprintf(buffer, sizeof(buffer), GEAR_LONG_FMT, (gear_long) t->us); break;

				case 'a': {
					if ((int) t->days != -99999) {
						length = slprintf(buffer, sizeof(buffer), "%d", (int) t->days);
					} else {
						length = slprintf(buffer, sizeof(buffer), "(unknown)");
					}
				} break;
				case 'r': length = slprintf(buffer, sizeof(buffer), "%s", t->invert ? "-" : ""); break;
				case 'R': length = slprintf(buffer, sizeof(buffer), "%c", t->invert ? '-' : '+'); break;

				case '%': length = slprintf(buffer, sizeof(buffer), "%%"); break;
				default: buffer[0] = '%'; buffer[1] = format[i]; buffer[2] = '\0'; length = 2; break;
			}
			smart_str_appendl(&string, buffer, length);
			have_format_spec = 0;
		} else {
			if (format[i] == '%') {
				have_format_spec = 1;
			} else {
				smart_str_appendc(&string, format[i]);
			}
		}
	}

	smart_str_0(&string);

	if (string.s == NULL) {
		return ZSTR_EMPTY_ALLOC();
	}

	return string.s;
}
/* }}} */

/* {{{ proto string date_interval_format(DateInterval object, string format)
   Formats the interval.
*/
HYSS_FUNCTION(date_interval_format)
{
	zval             *object;
	hyss_interval_obj *diobj;
	char             *format;
	size_t            format_len;

	if (gear_parse_method_parameters(GEAR_NUM_ARGS(), getThis(), "Os", &object, date_ce_interval, &format, &format_len) == FAILURE) {
		RETURN_FALSE;
	}
	diobj = Z_HYSSINTERVAL_P(object);
	DATE_CHECK_INITIALIZED(diobj->initialized, DateInterval);

	RETURN_STR(date_interval_format(format, format_len, diobj->diff));
}
/* }}} */

static int date_period_initialize(timelib_time **st, timelib_time **et, timelib_rel_time **d, gear_long *recurrences, /*const*/ char *format, size_t format_length) /* {{{ */
{
	timelib_time     *b = NULL, *e = NULL;
	timelib_rel_time *p = NULL;
	int               r = 0;
	int               retval = 0;
	timelib_error_container *errors;

	timelib_strtointerval(format, format_length, &b, &e, &p, &r, &errors);

	if (errors->error_count > 0) {
		hyss_error_docref(NULL, E_WARNING, "Unknown or bad format (%s)", format);
		retval = FAILURE;
	} else {
		*st = b;
		*et = e;
		*d  = p;
		*recurrences = r;
		retval = SUCCESS;
	}
	timelib_error_container_dtor(errors);
	return retval;
} /* }}} */

/* {{{ proto DatePeriod::__construct(DateTime $start, DateInterval $interval, int recurrences|DateTime $end)
   Creates new DatePeriod object.
*/
HYSS_METHOD(DatePeriod, __construct)
{
	hyss_period_obj   *dpobj;
	hyss_date_obj     *dateobj;
	zval *start, *end = NULL, *interval;
	gear_long  recurrences = 0, options = 0;
	char *isostr = NULL;
	size_t   isostr_len = 0;
	timelib_time *clone;
	gear_error_handling error_handling;

	gear_replace_error_handling(EH_THROW, NULL, &error_handling);
	if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, GEAR_NUM_ARGS(), "OOl|l", &start, date_ce_interface, &interval, date_ce_interval, &recurrences, &options) == FAILURE) {
		if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, GEAR_NUM_ARGS(), "OOO|l", &start, date_ce_interface, &interval, date_ce_interval, &end, date_ce_interface, &options) == FAILURE) {
			if (gear_parse_parameters_ex(GEAR_PARSE_PARAMS_QUIET, GEAR_NUM_ARGS(), "s|l", &isostr, &isostr_len, &options) == FAILURE) {
				hyss_error_docref(NULL, E_WARNING, "This constructor accepts either (DateTimeInterface, DateInterval, int) OR (DateTimeInterface, DateInterval, DateTime) OR (string) as arguments.");
				gear_restore_error_handling(&error_handling);
				return;
			}
		}
	}

	dpobj = Z_HYSSPERIOD_P(getThis());
	dpobj->current = NULL;

	if (isostr) {
		date_period_initialize(&(dpobj->start), &(dpobj->end), &(dpobj->interval), &recurrences, isostr, isostr_len);
		if (dpobj->start == NULL) {
			hyss_error_docref(NULL, E_WARNING, "The ISO interval '%s' did not contain a start date.", isostr);
		}
		if (dpobj->interval == NULL) {
			hyss_error_docref(NULL, E_WARNING, "The ISO interval '%s' did not contain an interval.", isostr);
		}
		if (dpobj->end == NULL && recurrences == 0) {
			hyss_error_docref(NULL, E_WARNING, "The ISO interval '%s' did not contain an end date or a recurrence count.", isostr);
		}

		if (dpobj->start) {
			timelib_update_ts(dpobj->start, NULL);
		}
		if (dpobj->end) {
			timelib_update_ts(dpobj->end, NULL);
		}
		dpobj->start_ce = date_ce_date;
	} else {
		/* init */
		hyss_interval_obj *intobj = Z_HYSSINTERVAL_P(interval);

		/* start date */
		dateobj = Z_HYSSDATE_P(start);
		clone = timelib_time_ctor();
		memcpy(clone, dateobj->time, sizeof(timelib_time));
		if (dateobj->time->tz_abbr) {
			clone->tz_abbr = timelib_strdup(dateobj->time->tz_abbr);
		}
		if (dateobj->time->tz_info) {
			clone->tz_info = dateobj->time->tz_info;
		}
		dpobj->start = clone;
		dpobj->start_ce = Z_OBJCE_P(start);

		/* interval */
		dpobj->interval = timelib_rel_time_clone(intobj->diff);

		/* end date */
		if (end) {
			dateobj = Z_HYSSDATE_P(end);
			clone = timelib_time_clone(dateobj->time);
			dpobj->end = clone;
		}
	}
 
	if (dpobj->end == NULL && recurrences < 1) {
		hyss_error_docref(NULL, E_WARNING, "The recurrence count '%d' is invalid. Needs to be > 0", (int) recurrences);
	}

	/* options */
	dpobj->include_start_date = !(options & HYSS_DATE_PERIOD_EXCLUDE_START_DATE);

	/* recurrrences */
	dpobj->recurrences = recurrences + dpobj->include_start_date;

	dpobj->initialized = 1;

	gear_restore_error_handling(&error_handling);
}
/* }}} */

/* {{{ proto DatePeriod::getStartDate()
   Get start date.
*/
HYSS_METHOD(DatePeriod, getStartDate)
{
	hyss_period_obj   *dpobj;
	hyss_date_obj     *dateobj;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	dpobj = Z_HYSSPERIOD_P(getThis());

        hyss_date_instantiate(dpobj->start_ce, return_value);
	dateobj = Z_HYSSDATE_P(return_value);
	dateobj->time = timelib_time_ctor();
	*dateobj->time = *dpobj->start;
	if (dpobj->start->tz_abbr) {
		dateobj->time->tz_abbr = timelib_strdup(dpobj->start->tz_abbr);
	}
	if (dpobj->start->tz_info) {
		dateobj->time->tz_info = dpobj->start->tz_info;
	}
}
/* }}} */

/* {{{ proto DatePeriod::getEndDate()
   Get end date.
*/
HYSS_METHOD(DatePeriod, getEndDate)
{
        hyss_period_obj   *dpobj;
        hyss_date_obj     *dateobj;

        if (gear_parse_parameters_none() == FAILURE) {
                return;
        }

        dpobj = Z_HYSSPERIOD_P(getThis());

        if (!dpobj->end) {
                return;
        }

        hyss_date_instantiate(dpobj->start_ce, return_value);
        dateobj = Z_HYSSDATE_P(return_value);
        dateobj->time = timelib_time_ctor();
        *dateobj->time = *dpobj->end;
        if (dpobj->end->tz_abbr) {
                dateobj->time->tz_abbr = timelib_strdup(dpobj->end->tz_abbr);
        }
        if (dpobj->end->tz_info) {
                dateobj->time->tz_info = dpobj->end->tz_info;
        }
}
/* }}} */

/* {{{ proto DatePeriod::getDateInterval()
   Get date interval.
*/
HYSS_METHOD(DatePeriod, getDateInterval)
{
	hyss_period_obj   *dpobj;
	hyss_interval_obj *diobj;

        if (gear_parse_parameters_none() == FAILURE) {
                return;
        }

	dpobj = Z_HYSSPERIOD_P(getThis());

	hyss_date_instantiate(date_ce_interval, return_value);
	diobj = Z_HYSSINTERVAL_P(return_value);
	diobj->diff = timelib_rel_time_clone(dpobj->interval);
	diobj->initialized = 1;
}
/* }}} */

/* {{{ proto int DatePeriod::getRecurrences()
   Get recurrences.
*/
HYSS_METHOD(DatePeriod, getRecurrences)
{
	hyss_period_obj *dpobj;

	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	dpobj = Z_HYSSPERIOD_P(getThis());

	if (0 == dpobj->recurrences - dpobj->include_start_date) {
		return;
	}

	RETURN_LONG(dpobj->recurrences - dpobj->include_start_date);
}
/* }}} */

static int check_id_allowed(char *id, gear_long what) /* {{{ */
{
	if (what & HYSS_DATE_TIMEZONE_GROUP_AFRICA     && strncasecmp(id, "Africa/",      7) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_AMERICA    && strncasecmp(id, "America/",     8) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_ANTARCTICA && strncasecmp(id, "Antarctica/", 11) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_ARCTIC     && strncasecmp(id, "Arctic/",      7) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_ASIA       && strncasecmp(id, "Asia/",        5) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_ATLANTIC   && strncasecmp(id, "Atlantic/",    9) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_AUSTRALIA  && strncasecmp(id, "Australia/",  10) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_EUROPE     && strncasecmp(id, "Europe/",      7) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_INDIAN     && strncasecmp(id, "Indian/",      7) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_PACIFIC    && strncasecmp(id, "Pacific/",     8) == 0) return 1;
	if (what & HYSS_DATE_TIMEZONE_GROUP_UTC        && strncasecmp(id, "UTC",          3) == 0) return 1;
	return 0;
} /* }}} */

/* {{{ proto array timezone_identifiers_list([long what[, string country]])
   Returns numerically index array with all timezone identifiers.
*/
HYSS_FUNCTION(timezone_identifiers_list)
{
	const timelib_tzdb             *tzdb;
	const timelib_tzdb_index_entry *table;
	int                             i, item_count;
	gear_long                       what = HYSS_DATE_TIMEZONE_GROUP_ALL;
	char                           *option = NULL;
	size_t                          option_len = 0;

	GEAR_PARSE_PARAMETERS_START(0, 2)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(what)
		Z_PARAM_STRING_EX(option, option_len, 1, 0)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	/* Extra validation */
	if (what == HYSS_DATE_TIMEZONE_PER_COUNTRY && option_len != 2) {
		hyss_error_docref(NULL, E_NOTICE, "A two-letter ISO 3166-1 compatible country code is expected");
		RETURN_FALSE;
	}

	tzdb = DATE_TIMEZONEDB;
	table = timelib_timezone_identifiers_list((timelib_tzdb*) tzdb, &item_count);

	array_init(return_value);

	for (i = 0; i < item_count; ++i) {
		if (what == HYSS_DATE_TIMEZONE_PER_COUNTRY) {
			if (tzdb->data[table[i].pos + 5] == option[0] && tzdb->data[table[i].pos + 6] == option[1]) {
				add_next_index_string(return_value, table[i].id);
			}
		} else if (what == HYSS_DATE_TIMEZONE_GROUP_ALL_W_BC || (check_id_allowed(table[i].id, what) && (tzdb->data[table[i].pos + 4] == '\1'))) {
			add_next_index_string(return_value, table[i].id);
		}
	};
}
/* }}} */

/* {{{ proto array timezone_version_get()
   Returns the Olson database version number.
*/
HYSS_FUNCTION(timezone_version_get)
{
	const timelib_tzdb *tzdb;

	tzdb = DATE_TIMEZONEDB;
	RETURN_STRING(tzdb->version);
}
/* }}} */

/* {{{ proto array timezone_abbreviations_list()
   Returns associative array containing dst, offset and the timezone name
*/
HYSS_FUNCTION(timezone_abbreviations_list)
{
	const timelib_tz_lookup_table *table, *entry;
	zval                          element, *abbr_array_p, abbr_array;

	table = timelib_timezone_abbreviations_list();
	array_init(return_value);
	entry = table;

	do {
		array_init(&element);
		add_assoc_bool_ex(&element, "dst", sizeof("dst") -1, entry->type);
		add_assoc_long_ex(&element, "offset", sizeof("offset") - 1, entry->gmtoffset);
		if (entry->full_tz_name) {
			add_assoc_string_ex(&element, "timezone_id", sizeof("timezone_id") - 1, entry->full_tz_name);
		} else {
			add_assoc_null_ex(&element, "timezone_id", sizeof("timezone_id") - 1);
		}

		abbr_array_p = gear_hash_str_find(Z_ARRVAL_P(return_value), entry->name, strlen(entry->name));
		if (!abbr_array_p) {
			array_init(&abbr_array);
			add_assoc_zval(return_value, entry->name, &abbr_array);
		} else {
			ZVAL_COPY_VALUE(&abbr_array, abbr_array_p);
		}
		add_next_index_zval(&abbr_array, &element);
		entry++;
	} while (entry->name);
}
/* }}} */

/* {{{ proto bool date_default_timezone_set(string timezone_identifier)
   Sets the default timezone used by all date/time functions in a script */
HYSS_FUNCTION(date_default_timezone_set)
{
	char *zone;
	size_t   zone_len;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_STRING(zone, zone_len)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	if (!timelib_timezone_id_is_valid(zone, DATE_TIMEZONEDB)) {
		hyss_error_docref(NULL, E_NOTICE, "Timezone ID '%s' is invalid", zone);
		RETURN_FALSE;
	}
	if (DATEG(timezone)) {
		efree(DATEG(timezone));
		DATEG(timezone) = NULL;
	}
	DATEG(timezone) = estrndup(zone, zone_len);
	RETURN_TRUE;
}
/* }}} */

/* {{{ proto string date_default_timezone_get()
   Gets the default timezone used by all date/time functions in a script */
HYSS_FUNCTION(date_default_timezone_get)
{
	timelib_tzinfo *default_tz;
	if (gear_parse_parameters_none() == FAILURE) {
		return;
	}

	default_tz = get_timezone_info();
	RETVAL_STRING(default_tz->name);
}
/* }}} */

/* {{{ hyss_do_date_sunrise_sunset
 *  Common for date_sunrise() and date_sunset() functions
 */
static void hyss_do_date_sunrise_sunset(INTERNAL_FUNCTION_PARAMETERS, int calc_sunset)
{
	double latitude = 0.0, longitude = 0.0, zenith = 0.0, gmt_offset = 0, altitude;
	double h_rise, h_set, N;
	timelib_sll rise, set, transit;
	gear_long time, retformat = 0;
	int             rs;
	timelib_time   *t;
	timelib_tzinfo *tzi;
	gear_string    *retstr;

	GEAR_PARSE_PARAMETERS_START(1, 6)
		Z_PARAM_LONG(time)
		Z_PARAM_OPTIONAL
		Z_PARAM_LONG(retformat)
		Z_PARAM_DOUBLE(latitude)
		Z_PARAM_DOUBLE(longitude)
		Z_PARAM_DOUBLE(zenith)
		Z_PARAM_DOUBLE(gmt_offset)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	switch (GEAR_NUM_ARGS()) {
		case 1:
			retformat = SUNFUNCS_RET_STRING;
		case 2:
			latitude = ICS_FLT("date.default_latitude");
		case 3:
			longitude = ICS_FLT("date.default_longitude");
		case 4:
			if (calc_sunset) {
				zenith = ICS_FLT("date.sunset_zenith");
			} else {
				zenith = ICS_FLT("date.sunrise_zenith");
			}
		case 5:
		case 6:
			break;
		default:
			hyss_error_docref(NULL, E_WARNING, "invalid format");
			RETURN_FALSE;
			break;
	}
	if (retformat != SUNFUNCS_RET_TIMESTAMP &&
		retformat != SUNFUNCS_RET_STRING &&
		retformat != SUNFUNCS_RET_DOUBLE)
	{
		hyss_error_docref(NULL, E_WARNING, "Wrong return format given, pick one of SUNFUNCS_RET_TIMESTAMP, SUNFUNCS_RET_STRING or SUNFUNCS_RET_DOUBLE");
		RETURN_FALSE;
	}
	altitude = 90 - zenith;

	/* Initialize time struct */
	t = timelib_time_ctor();
	tzi = get_timezone_info();
	t->tz_info = tzi;
	t->zone_type = TIMELIB_ZONETYPE_ID;

	if (GEAR_NUM_ARGS() <= 5) {
		gmt_offset = timelib_get_current_offset(t) / 3600;
	}

	timelib_unixtime2local(t, time);
	rs = timelib_astro_rise_set_altitude(t, longitude, latitude, altitude, 1, &h_rise, &h_set, &rise, &set, &transit);
	timelib_time_dtor(t);

	if (rs != 0) {
		RETURN_FALSE;
	}

	if (retformat == SUNFUNCS_RET_TIMESTAMP) {
		RETURN_LONG(calc_sunset ? set : rise);
	}
	N = (calc_sunset ? h_set : h_rise) + gmt_offset;

	if (N > 24 || N < 0) {
		N -= floor(N / 24) * 24;
	}

	switch (retformat) {
		case SUNFUNCS_RET_STRING:
			retstr = strpprintf(0, "%02d:%02d", (int) N, (int) (60 * (N - (int) N)));
			RETURN_NEW_STR(retstr);
			break;
		case SUNFUNCS_RET_DOUBLE:
			RETURN_DOUBLE(N);
			break;
	}
}
/* }}} */

/* {{{ proto mixed date_sunrise(mixed time [, int format [, float latitude [, float longitude [, float zenith [, float gmt_offset]]]]])
   Returns time of sunrise for a given day and location */
HYSS_FUNCTION(date_sunrise)
{
	hyss_do_date_sunrise_sunset(INTERNAL_FUNCTION_PARAM_PASSTHRU, 0);
}
/* }}} */

/* {{{ proto mixed date_sunset(mixed time [, int format [, float latitude [, float longitude [, float zenith [, float gmt_offset]]]]])
   Returns time of sunset for a given day and location */
HYSS_FUNCTION(date_sunset)
{
	hyss_do_date_sunrise_sunset(INTERNAL_FUNCTION_PARAM_PASSTHRU, 1);
}
/* }}} */

/* {{{ proto array date_sun_info(int time, float latitude, float longitude)
   Returns an array with information about sun set/rise and twilight begin/end */
HYSS_FUNCTION(date_sun_info)
{
	gear_long       time;
	double          latitude, longitude;
	timelib_time   *t, *t2;
	timelib_tzinfo *tzi;
	int             rs;
	timelib_sll     rise, set, transit;
	int             dummy;
	double          ddummy;

	GEAR_PARSE_PARAMETERS_START(3, 3)
		Z_PARAM_LONG(time)
		Z_PARAM_DOUBLE(latitude)
		Z_PARAM_DOUBLE(longitude)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	/* Initialize time struct */
	t = timelib_time_ctor();
	tzi = get_timezone_info();
	t->tz_info = tzi;
	t->zone_type = TIMELIB_ZONETYPE_ID;
	timelib_unixtime2local(t, time);

	/* Setup */
	t2 = timelib_time_ctor();
	array_init(return_value);

	/* Get sun up/down and transit */
	rs = timelib_astro_rise_set_altitude(t, longitude, latitude, -35.0/60, 1, &ddummy, &ddummy, &rise, &set, &transit);
	switch (rs) {
		case -1: /* always below */
			add_assoc_bool(return_value, "sunrise", 0);
			add_assoc_bool(return_value, "sunset", 0);
			break;
		case 1: /* always above */
			add_assoc_bool(return_value, "sunrise", 1);
			add_assoc_bool(return_value, "sunset", 1);
			break;
		default:
			t2->sse = rise;
			add_assoc_long(return_value, "sunrise", timelib_date_to_int(t2, &dummy));
			t2->sse = set;
			add_assoc_long(return_value, "sunset", timelib_date_to_int(t2, &dummy));
	}
	t2->sse = transit;
	add_assoc_long(return_value, "transit", timelib_date_to_int(t2, &dummy));

	/* Get civil twilight */
	rs = timelib_astro_rise_set_altitude(t, longitude, latitude, -6.0, 0, &ddummy, &ddummy, &rise, &set, &transit);
	switch (rs) {
		case -1: /* always below */
			add_assoc_bool(return_value, "civil_twilight_begin", 0);
			add_assoc_bool(return_value, "civil_twilight_end", 0);
			break;
		case 1: /* always above */
			add_assoc_bool(return_value, "civil_twilight_begin", 1);
			add_assoc_bool(return_value, "civil_twilight_end", 1);
			break;
		default:
			t2->sse = rise;
			add_assoc_long(return_value, "civil_twilight_begin", timelib_date_to_int(t2, &dummy));
			t2->sse = set;
			add_assoc_long(return_value, "civil_twilight_end", timelib_date_to_int(t2, &dummy));
	}

	/* Get nautical twilight */
	rs = timelib_astro_rise_set_altitude(t, longitude, latitude, -12.0, 0, &ddummy, &ddummy, &rise, &set, &transit);
	switch (rs) {
		case -1: /* always below */
			add_assoc_bool(return_value, "nautical_twilight_begin", 0);
			add_assoc_bool(return_value, "nautical_twilight_end", 0);
			break;
		case 1: /* always above */
			add_assoc_bool(return_value, "nautical_twilight_begin", 1);
			add_assoc_bool(return_value, "nautical_twilight_end", 1);
			break;
		default:
			t2->sse = rise;
			add_assoc_long(return_value, "nautical_twilight_begin", timelib_date_to_int(t2, &dummy));
			t2->sse = set;
			add_assoc_long(return_value, "nautical_twilight_end", timelib_date_to_int(t2, &dummy));
	}

	/* Get astronomical twilight */
	rs = timelib_astro_rise_set_altitude(t, longitude, latitude, -18.0, 0, &ddummy, &ddummy, &rise, &set, &transit);
	switch (rs) {
		case -1: /* always below */
			add_assoc_bool(return_value, "astronomical_twilight_begin", 0);
			add_assoc_bool(return_value, "astronomical_twilight_end", 0);
			break;
		case 1: /* always above */
			add_assoc_bool(return_value, "astronomical_twilight_begin", 1);
			add_assoc_bool(return_value, "astronomical_twilight_end", 1);
			break;
		default:
			t2->sse = rise;
			add_assoc_long(return_value, "astronomical_twilight_begin", timelib_date_to_int(t2, &dummy));
			t2->sse = set;
			add_assoc_long(return_value, "astronomical_twilight_end", timelib_date_to_int(t2, &dummy));
	}
	timelib_time_dtor(t);
	timelib_time_dtor(t2);
}
/* }}} */

static HashTable *date_object_get_gc_period(zval *object, zval **table, int *n) /* {{{ */
{
	*table = NULL;
	*n = 0;
	return gear_std_get_properties(object);
} /* }}} */

static HashTable *date_object_get_properties_period(zval *object) /* {{{ */
{
	HashTable		*props;
	zval			 zv;
	hyss_period_obj	*period_obj;

	period_obj = Z_HYSSPERIOD_P(object);

	props = gear_std_get_properties(object);

	if (!period_obj->start) {
		return props;
	}

	if (period_obj->start) {
		hyss_date_obj *date_obj;
		object_init_ex(&zv, date_ce_date);
		date_obj = Z_HYSSDATE_P(&zv);
		date_obj->time = timelib_time_clone(period_obj->start);
	} else {
		ZVAL_NULL(&zv);
	}
	gear_hash_str_update(props, "start", sizeof("start")-1, &zv);

	if (period_obj->current) {
		hyss_date_obj *date_obj;
		object_init_ex(&zv, date_ce_date);
		date_obj = Z_HYSSDATE_P(&zv);
		date_obj->time = timelib_time_clone(period_obj->current);
	} else {
		ZVAL_NULL(&zv);
	}
	gear_hash_str_update(props, "current", sizeof("current")-1, &zv);

	if (period_obj->end) {
		hyss_date_obj *date_obj;
		object_init_ex(&zv, date_ce_date);
		date_obj = Z_HYSSDATE_P(&zv);
		date_obj->time = timelib_time_clone(period_obj->end);
	} else {
		ZVAL_NULL(&zv);
	}
	gear_hash_str_update(props, "end", sizeof("end")-1, &zv);

	if (period_obj->interval) {
		hyss_interval_obj *interval_obj;
		object_init_ex(&zv, date_ce_interval);
		interval_obj = Z_HYSSINTERVAL_P(&zv);
		interval_obj->diff = timelib_rel_time_clone(period_obj->interval);
		interval_obj->initialized = 1;
	} else {
		ZVAL_NULL(&zv);
	}
	gear_hash_str_update(props, "interval", sizeof("interval")-1, &zv);

	/* converted to larger type (int->long); must check when unserializing */
	ZVAL_LONG(&zv, (gear_long) period_obj->recurrences);
	gear_hash_str_update(props, "recurrences", sizeof("recurrences")-1, &zv);

	ZVAL_BOOL(&zv, period_obj->include_start_date);
	gear_hash_str_update(props, "include_start_date", sizeof("include_start_date")-1, &zv);

	return props;
} /* }}} */

static int hyss_date_period_initialize_from_hash(hyss_period_obj *period_obj, HashTable *myht) /* {{{ */
{
	zval *ht_entry;

	/* this function does no rollback on error */

	ht_entry = gear_hash_str_find(myht, "start", sizeof("start")-1);
	if (ht_entry) {
		if (Z_TYPE_P(ht_entry) == IS_OBJECT && Z_OBJCE_P(ht_entry) == date_ce_date) {
			hyss_date_obj *date_obj;
			date_obj = Z_HYSSDATE_P(ht_entry);
			period_obj->start = timelib_time_clone(date_obj->time);
			period_obj->start_ce = Z_OBJCE_P(ht_entry);
		} else if (Z_TYPE_P(ht_entry) != IS_NULL) {
			return 0;
		}
	} else {
		return 0;
	}

	ht_entry = gear_hash_str_find(myht, "end", sizeof("end")-1);
	if (ht_entry) {
		if (Z_TYPE_P(ht_entry) == IS_OBJECT && Z_OBJCE_P(ht_entry) == date_ce_date) {
			hyss_date_obj *date_obj;
			date_obj = Z_HYSSDATE_P(ht_entry);
			period_obj->end = timelib_time_clone(date_obj->time);
		} else if (Z_TYPE_P(ht_entry) != IS_NULL) {
			return 0;
		}
	} else {
		return 0;
	}

	ht_entry = gear_hash_str_find(myht, "current", sizeof("current")-1);
	if (ht_entry) {
		if (Z_TYPE_P(ht_entry) == IS_OBJECT && Z_OBJCE_P(ht_entry) == date_ce_date) {
			hyss_date_obj *date_obj;
			date_obj = Z_HYSSDATE_P(ht_entry);
			period_obj->current = timelib_time_clone(date_obj->time);
		} else if (Z_TYPE_P(ht_entry) != IS_NULL)  {
			return 0;
		}
	} else {
		return 0;
	}

	ht_entry = gear_hash_str_find(myht, "interval", sizeof("interval")-1);
	if (ht_entry) {
		if (Z_TYPE_P(ht_entry) == IS_OBJECT && Z_OBJCE_P(ht_entry) == date_ce_interval) {
			hyss_interval_obj *interval_obj;
			interval_obj = Z_HYSSINTERVAL_P(ht_entry);
			period_obj->interval = timelib_rel_time_clone(interval_obj->diff);
		} else { /* interval is required */
			return 0;
		}
	} else {
		return 0;
	}

	ht_entry = gear_hash_str_find(myht, "recurrences", sizeof("recurrences")-1);
	if (ht_entry &&
			Z_TYPE_P(ht_entry) == IS_LONG && Z_LVAL_P(ht_entry) >= 0 && Z_LVAL_P(ht_entry) <= INT_MAX) {
		period_obj->recurrences = Z_LVAL_P(ht_entry);
	} else {
		return 0;
	}

	ht_entry = gear_hash_str_find(myht, "include_start_date", sizeof("include_start_date")-1);
	if (ht_entry &&
			(Z_TYPE_P(ht_entry) == IS_FALSE || Z_TYPE_P(ht_entry) == IS_TRUE)) {
		period_obj->include_start_date = (Z_TYPE_P(ht_entry) == IS_TRUE);
	} else {
		return 0;
	}

	period_obj->initialized = 1;

	return 1;
} /* }}} */

/* {{{ proto DatePeriod::__set_state(array array)
*/
HYSS_METHOD(DatePeriod, __set_state)
{
	hyss_period_obj   *period_obj;
	zval             *array;
	HashTable        *myht;

	GEAR_PARSE_PARAMETERS_START(1, 1)
		Z_PARAM_ARRAY(array)
	GEAR_PARSE_PARAMETERS_END_EX(RETURN_FALSE);

	myht = Z_ARRVAL_P(array);

	object_init_ex(return_value, date_ce_period);
	period_obj = Z_HYSSPERIOD_P(return_value);
	if (!hyss_date_period_initialize_from_hash(period_obj, myht)) {
		gear_throw_error(NULL, "Invalid serialization data for DatePeriod object");
	}
}
/* }}} */

/* {{{ proto DatePeriod::__wakeup()
*/
HYSS_METHOD(DatePeriod, __wakeup)
{
	zval             *object = getThis();
	hyss_period_obj   *period_obj;
	HashTable        *myht;

	period_obj = Z_HYSSPERIOD_P(object);

	myht = Z_OBJPROP_P(object);

	if (!hyss_date_period_initialize_from_hash(period_obj, myht)) {
		gear_throw_error(NULL, "Invalid serialization data for DatePeriod object");
	}
}
/* }}} */

/* {{{ date_period_read_property */
static zval *date_period_read_property(zval *object, zval *member, int type, void **cache_slot, zval *rv)
{
	if (type != BP_VAR_IS && type != BP_VAR_R) {
		gear_throw_error(NULL, "Retrieval of DatePeriod properties for modification is unsupported");
		return &EG(uninitialized_zval);
	}

	Z_OBJPROP_P(object); /* build properties hash table */

	return gear_std_read_property(object, member, type, cache_slot, rv);
}
/* }}} */

/* {{{ date_period_write_property */
static void date_period_write_property(zval *object, zval *member, zval *value, void **cache_slot)
{
	gear_throw_error(NULL, "Writing to DatePeriod properties is unsupported");
}
/* }}} */

