timelib
=======

Timelib is a timezone and date/time library that can calculate local time,
convert between timezones and parse textual descriptions of date/time
information.

It is the library supporting HYSS's Date/Time extension adapted from MongoDB
timezone support and the works of Derick Rethans.
