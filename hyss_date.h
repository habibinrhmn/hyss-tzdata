/* HySS - Hyang Server Scripts
 *
 * Copyright (C) 2019-2020 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYSS_DATE_H
#define HYSS_DATE_H

#include "lib/timelib.h"
#include "Gear/gear_hash.h"

#include "hyss_version.h"
#define HYSS_DATE_VERSION HYSS_VERSION

extern gear_capi_entry date_capi_entry;
#define hyssext_date_ptr &date_capi_entry

HYSS_FUNCTION(date);
HYSS_FUNCTION(idate);
HYSS_FUNCTION(gmdate);
HYSS_FUNCTION(strtotime);

HYSS_FUNCTION(mktime);
HYSS_FUNCTION(gmmktime);

HYSS_FUNCTION(checkdate);

#ifdef HAVE_STRFTIME
HYSS_FUNCTION(strftime);
HYSS_FUNCTION(gmstrftime);
#endif

HYSS_FUNCTION(time);
HYSS_FUNCTION(localtime);
HYSS_FUNCTION(getdate);

/* Advanced Interface */
HYSS_METHOD(DateTime, __construct);
HYSS_METHOD(DateTime, __wakeup);
HYSS_METHOD(DateTime, __set_state);
HYSS_METHOD(DateTime, createFromImmutable);
HYSS_FUNCTION(date_create);
HYSS_FUNCTION(date_create_immutable);
HYSS_FUNCTION(date_create_from_format);
HYSS_FUNCTION(date_create_immutable_from_format);
HYSS_FUNCTION(date_parse);
HYSS_FUNCTION(date_parse_from_format);
HYSS_FUNCTION(date_get_last_errors);
HYSS_FUNCTION(date_format);
HYSS_FUNCTION(date_modify);
HYSS_FUNCTION(date_add);
HYSS_FUNCTION(date_sub);
HYSS_FUNCTION(date_timezone_get);
HYSS_FUNCTION(date_timezone_set);
HYSS_FUNCTION(date_offset_get);
HYSS_FUNCTION(date_diff);

HYSS_FUNCTION(date_time_set);
HYSS_FUNCTION(date_date_set);
HYSS_FUNCTION(date_isodate_set);
HYSS_FUNCTION(date_timestamp_set);
HYSS_FUNCTION(date_timestamp_get);

HYSS_METHOD(DateTimeImmutable, __construct);
HYSS_METHOD(DateTimeImmutable, __set_state);
HYSS_METHOD(DateTimeImmutable, modify);
HYSS_METHOD(DateTimeImmutable, add);
HYSS_METHOD(DateTimeImmutable, sub);
HYSS_METHOD(DateTimeImmutable, setTimezone);
HYSS_METHOD(DateTimeImmutable, setTime);
HYSS_METHOD(DateTimeImmutable, setDate);
HYSS_METHOD(DateTimeImmutable, setISODate);
HYSS_METHOD(DateTimeImmutable, setTimestamp);
HYSS_METHOD(DateTimeImmutable, createFromMutable);

HYSS_METHOD(DateTimeZone, __construct);
HYSS_METHOD(DateTimeZone, __wakeup);
HYSS_METHOD(DateTimeZone, __set_state);
HYSS_FUNCTION(timezone_open);
HYSS_FUNCTION(timezone_name_get);
HYSS_FUNCTION(timezone_name_from_abbr);
HYSS_FUNCTION(timezone_offset_get);
HYSS_FUNCTION(timezone_transitions_get);
HYSS_FUNCTION(timezone_location_get);
HYSS_FUNCTION(timezone_identifiers_list);
HYSS_FUNCTION(timezone_abbreviations_list);
HYSS_FUNCTION(timezone_version_get);

HYSS_METHOD(DateInterval, __construct);
HYSS_METHOD(DateInterval, __wakeup);
HYSS_METHOD(DateInterval, __set_state);
HYSS_FUNCTION(date_interval_format);
HYSS_FUNCTION(date_interval_create_from_date_string);

HYSS_METHOD(DatePeriod, __construct);
HYSS_METHOD(DatePeriod, __wakeup);
HYSS_METHOD(DatePeriod, __set_state);
HYSS_METHOD(DatePeriod, getStartDate);
HYSS_METHOD(DatePeriod, getEndDate);
HYSS_METHOD(DatePeriod, getDateInterval);
HYSS_METHOD(DatePeriod, getRecurrences);

/* Options and Configuration */
HYSS_FUNCTION(date_default_timezone_set);
HYSS_FUNCTION(date_default_timezone_get);

/* Astro functions */
HYSS_FUNCTION(date_sunrise);
HYSS_FUNCTION(date_sunset);
HYSS_FUNCTION(date_sun_info);

HYSS_RINIT_FUNCTION(date);
HYSS_RSHUTDOWN_FUNCTION(date);
HYSS_MINIT_FUNCTION(date);
HYSS_MSHUTDOWN_FUNCTION(date);
HYSS_MINFO_FUNCTION(date);

typedef struct _hyss_date_obj hyss_date_obj;
typedef struct _hyss_timezone_obj hyss_timezone_obj;
typedef struct _hyss_interval_obj hyss_interval_obj;
typedef struct _hyss_period_obj hyss_period_obj;

struct _hyss_date_obj {
	timelib_time *time;
	HashTable    *props;
	gear_object   std;
};

static inline hyss_date_obj *hyss_date_obj_from_obj(gear_object *obj) {
	return (hyss_date_obj*)((char*)(obj) - XtOffsetOf(hyss_date_obj, std));
}

#define Z_HYSSDATE_P(zv)  hyss_date_obj_from_obj(Z_OBJ_P((zv)))

struct _hyss_timezone_obj {
	int             initialized;
	int             type;
	union {
		timelib_tzinfo   *tz;         /* TIMELIB_ZONETYPE_ID */
		timelib_sll       utc_offset; /* TIMELIB_ZONETYPE_OFFSET */
		timelib_abbr_info z;          /* TIMELIB_ZONETYPE_ABBR */
	} tzi;
	HashTable *props;
	gear_object std;
};

static inline hyss_timezone_obj *hyss_timezone_obj_from_obj(gear_object *obj) {
	return (hyss_timezone_obj*)((char*)(obj) - XtOffsetOf(hyss_timezone_obj, std));
}

#define Z_HYSSTIMEZONE_P(zv)  hyss_timezone_obj_from_obj(Z_OBJ_P((zv)))

struct _hyss_interval_obj {
	timelib_rel_time *diff;
	HashTable        *props;
	int               initialized;
	gear_object       std;
};

static inline hyss_interval_obj *hyss_interval_obj_from_obj(gear_object *obj) {
	return (hyss_interval_obj*)((char*)(obj) - XtOffsetOf(hyss_interval_obj, std));
}

#define Z_HYSSINTERVAL_P(zv)  hyss_interval_obj_from_obj(Z_OBJ_P((zv)))

struct _hyss_period_obj {
	timelib_time     *start;
	gear_class_entry *start_ce;
	timelib_time     *current;
	timelib_time     *end;
	timelib_rel_time *interval;
	int               recurrences;
	int               initialized;
	int               include_start_date;
	gear_object       std;
};

static inline hyss_period_obj *hyss_period_obj_from_obj(gear_object *obj) {
	return (hyss_period_obj*)((char*)(obj) - XtOffsetOf(hyss_period_obj, std));
}

#define Z_HYSSPERIOD_P(zv)  hyss_period_obj_from_obj(Z_OBJ_P((zv)))

GEAR_BEGIN_CAPI_GLOBALS(date)
	char                    *default_timezone;
	char                    *timezone;
	HashTable               *tzcache;
	timelib_error_container *last_errors;
	int                     timezone_valid;
GEAR_END_CAPI_GLOBALS(date)

#define DATEG(v) GEAR_CAPI_GLOBALS_ACCESSOR(date, v)

/* Backwards compatibility wrapper */
HYSSAPI gear_long hyss_parse_date(char *string, gear_long *now);
HYSSAPI void hyss_mktime(INTERNAL_FUNCTION_PARAMETERS, int gmt);
HYSSAPI int hyss_idate(char format, time_t ts, int localtime);
#if HAVE_STRFTIME
#define _hyss_strftime hyss_strftime
HYSSAPI void hyss_strftime(INTERNAL_FUNCTION_PARAMETERS, int gm);
#endif
HYSSAPI gear_string *hyss_format_date(char *format, size_t format_len, time_t ts, int localtime);

/* Mechanism to set new TZ database */
HYSSAPI void hyss_date_set_tzdb(timelib_tzdb *tzdb);
HYSSAPI timelib_tzinfo *get_timezone_info(void);

/* Grabbing CE's so that other exts can use the date objects too */
HYSSAPI gear_class_entry *hyss_date_get_date_ce(void);
HYSSAPI gear_class_entry *hyss_date_get_immutable_ce(void);
HYSSAPI gear_class_entry *hyss_date_get_interface_ce(void);
HYSSAPI gear_class_entry *hyss_date_get_timezone_ce(void);
HYSSAPI gear_class_entry *hyss_date_get_interval_ce(void);
HYSSAPI gear_class_entry *hyss_date_get_period_ce(void);

/* Functions for creating DateTime objects, and initializing them from a string */
HYSSAPI zval *hyss_date_instantiate(gear_class_entry *pce, zval *object);
HYSSAPI int hyss_date_initialize(hyss_date_obj *dateobj, /*const*/ char *time_str, size_t time_str_len, char *format, zval *timezone_object, int ctor);


#endif /* HYSS_DATE_H */
